/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct{
	char MPU6050_ADDR;
	char WHO_AM_I_REG;
	char SMPLRT_DIV_REG;
	char GYRO_CONFIG_REG;
	char ACCEL_CONFIG_REF;
	char ACCEL_XOUT_H_REG;
	char TEMP_OUT_H_REG;
	char GYRO_XOUT_H_REG ;
	char PWR_MGMT_1_REG;

	uint16_t Accel_x_raw;
	uint16_t Accel_y_raw;
	uint16_t Accel_z_raw;
	double Ax;
	double Ay;
    double Az;

	uint16_t Gyro_x_raw;
	uint16_t Gyro_y_raw;
	uint16_t Gyro_z_raw;
	double Gx;
	double Gy;
	double Gz;
} GyroAcc;

GyroAcc gyroaccel;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
int Timeout = 1000;
int Delay = 1000;

uint8_t enter[] = "\r\n";
uint8_t tab[] = "\t";
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void MPU6050_init(void){
	gyroaccel.MPU6050_ADDR = 0xD0;
	gyroaccel.WHO_AM_I_REG = 0x75;
	gyroaccel.SMPLRT_DIV_REG = 0x19;
    gyroaccel.GYRO_CONFIG_REG = 0x1B;
	gyroaccel.ACCEL_CONFIG_REF = 0x1C;
	gyroaccel.ACCEL_XOUT_H_REG = 0x3B;
	gyroaccel.TEMP_OUT_H_REG = 0x41;
	gyroaccel.GYRO_XOUT_H_REG = 0x43;
	gyroaccel.PWR_MGMT_1_REG = 0x6B;

	uint8_t check, data;

	HAL_I2C_Mem_Read(&hi2c1, gyroaccel.MPU6050_ADDR, gyroaccel.WHO_AM_I_REG, 1, &check, 1, Timeout);

	if(check == 104){
		data = 0;
		HAL_I2C_Mem_Write(&hi2c1, gyroaccel.MPU6050_ADDR, gyroaccel.PWR_MGMT_1_REG, 1, &data, 1, Timeout);

		data = 0x07;
		HAL_I2C_Mem_Write(&hi2c1, gyroaccel.MPU6050_ADDR, gyroaccel.SMPLRT_DIV_REG, 1, &data, 1, Timeout);

		data = 0x00;
		HAL_I2C_Mem_Write(&hi2c1, gyroaccel.MPU6050_ADDR, gyroaccel.GYRO_CONFIG_REG, 1, &data, 1, Timeout);

		data = 0x00;
		HAL_I2C_Mem_Write(&hi2c1, gyroaccel.MPU6050_ADDR, gyroaccel.ACCEL_CONFIG_REF, 1, &data, 1, Timeout);
	}
}

void MPU6050_accel(void){
	uint8_t rec_data[6];
	char buf[50];
	HAL_I2C_Mem_Read(&hi2c1, gyroaccel.MPU6050_ADDR, gyroaccel.ACCEL_XOUT_H_REG, 1, rec_data, 6, Timeout);

	gyroaccel.Accel_x_raw = (uint16_t)(rec_data[0] << 8 | rec_data[1]);
	gyroaccel.Accel_x_raw = (uint16_t)(rec_data[2] << 8 | rec_data[3]);
	gyroaccel.Accel_x_raw = (uint16_t)(rec_data[4] << 8 | rec_data[5]);

	gyroaccel.Ax = gyroaccel.Accel_x_raw/16386.0;
	gyroaccel.Ay = gyroaccel.Accel_y_raw/16386.0;
	gyroaccel.Az = gyroaccel.Accel_z_raw/16386.0;

	sprintf(buf, "Ax: %d | Ay: %d | Az: %d", gyroaccel.Ax, gyroaccel.Ay, gyroaccel.Az);
	HAL_UART_Transmit(&huart1, (uint8_t *)&buf, sizeof(buf), Timeout);
	HAL_UART_Transmit(&huart1, tab, sizeof(tab), Timeout);
}

void MPU6050_gyro(void){
	uint8_t rec_data[6];
	char buf[50];

	HAL_I2C_Mem_Read(&hi2c1, gyroaccel.MPU6050_ADDR, gyroaccel.GYRO_XOUT_H_REG, 1, rec_data, 6, Timeout);

	gyroaccel.Gyro_x_raw = (uint16_t)(rec_data[0] << 8 | rec_data[1]);
	gyroaccel.Gyro_x_raw = (uint16_t)(rec_data[2] << 8 | rec_data[3]);
	gyroaccel.Gyro_x_raw = (uint16_t)(rec_data[4] << 8 | rec_data[5]);

	gyroaccel.Gx = gyroaccel.Gyro_x_raw/131.0;
	gyroaccel.Gy = gyroaccel.Gyro_y_raw/131.0;
	gyroaccel.Gz = gyroaccel.Gyro_z_raw/131.0;

	sprintf(buf, "Gx: %d | Gy: %d | Gz: %d", gyroaccel.Gx, gyroaccel.Gy, gyroaccel.Gz);
	HAL_UART_Transmit(&huart1, (uint8_t *)&buf, sizeof(buf), Timeout);
	HAL_UART_Transmit(&huart1, (uint8_t *)&enter, sizeof(enter), Timeout);
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
  uint8_t msg[] = "Unit gyroscope and accelerometer started!\r\n";
  HAL_UART_Transmit(&huart1, msg, sizeof(msg), Timeout);
  MPU6050_init();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  MPU6050_accel();
	  MPU6050_gyro();
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  HAL_Delay(Delay);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
