/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct{
	char spi_cntrl; // register control for tc72 for write address 0x80
	char spi_os; // register for tc72 at one-shot temperature after that it goes into low power mode
	char spi_read_msb; // register to read MSB temp
	char spi_read_lsb; // register to read LSB temp
	char spi_shdn; // register to shutdown tc72

	volatile unsigned short spi_temp_msb; // variable to store the MSB of the temperature
	volatile unsigned short spi_temp_lsb; // variable to store the LSB of the temperature

	double temperatuur; // variable to caculate the actual temperature

	uint8_t temperatuur_tx[20]; // buffer variable to send via uart
	uint16_t msb_plus_lsb; // variable to store MSB and LSB values into correct orde

} Temp;

Temp temp;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
int Timeout = 100;

int Delay = 1000;

uint8_t enter[] = "\r\n";
uint8_t tab[] = "\t";
uint8_t greetings[] = "Unit temp is ready\r\n";

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_SPI1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/*
 * Function to configure one shot temperature from TC72
 * The TC72 will be activate to get temp value
 * and after that it will be put into low power mode
 * First write 0x80 to control the TC72
 * Second write 0x15 to configure TC72 in one shot mode
 */
void SetupOneshotTC72(){
	/* setup registers for TC72 */
	temp.spi_cntrl = 0x80; // control register see datasheet table 4-1
	temp.spi_os = 0x15; // ob00010101 = 0x15 ==> control + one-shot + shutdown register - datasheet table 4-1
	/* activate chip select */
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, SET); // set cs high
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, RESET); // set cs low to activate
	/* send registers to configure one shot to TC72 */
	HAL_SPI_Transmit(&hspi1, (uint8_t *)&temp.spi_cntrl, sizeof(temp.spi_cntrl), Timeout); // write control register to TC72
	HAL_SPI_Transmit(&hspi1, (uint8_t *)&temp.spi_os, sizeof(temp.spi_os), Timeout); // write one-shot register to TC72
	/* deactivate chip select */
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, SET); // set cs high
}

/*
 * Function to obtain the temp value in MSB and LSB form
 * Example: 41.5 Celsius ==> MSB = 41, LSB = .5
 * First write 0x02 to TC72 to read MSB temperature
 * Second write 0x01 to TC72 to read LSB temperature
 * After that put MSB and LSB into a buffer
 */
void GetMSBtempAndLSBTempFromTC72(){
	SetupOneshotTC72();
	/* setup registers for TC72 */
	temp.spi_read_msb = 0x02; // read address to read msb
	temp.spi_read_lsb = 0x01; // read address to read lsb
	temp.spi_shdn = 0x01; // write adress to shut down
	/* activate chip select */
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, RESET);
	/* write and read via spi to TC72 */
	HAL_SPI_TransmitReceive(&hspi1, (uint8_t *)&temp.spi_read_msb, (uint8_t *)&temp.spi_temp_msb, sizeof(temp.spi_temp_msb), Timeout); // write to read msb value
	HAL_SPI_TransmitReceive(&hspi1, (uint8_t *)&temp.spi_read_lsb, (uint8_t *)&temp.spi_temp_lsb, sizeof(temp.spi_temp_lsb), Timeout); // write to read lsb value
	temp.msb_plus_lsb = (temp.spi_temp_msb & 0xff00) | (temp.spi_temp_lsb & 0x00ff);
	/* calculating the temp value when temp.spi_os = 0x40 */
//	temp.temperatuur = (temp.msb_plus_lsb / 128); // when temp.spi_os = 0x40;
//	utoa(temp.temperatuur, (char *)temp.temperatuur_tx, 10); // store the calculated temperature into a buffer
	/* calculating the temp value when temp.spi_os = 0x15 */
	temp.temperatuur = ((temp.msb_plus_lsb - 32768)/128); // the value from TC72 minus the zero reference point and divide by 128
	utoa(temp.temperatuur, (char *)temp.temperatuur_tx, 10); // store the calculated temperature into  a buffer
	/* display the calculated temp value to UART */
	uint8_t temp_msg[] = "De temperatuur in graden Celsius is: ";
	HAL_UART_Transmit(&huart1, temp_msg, sizeof(temp_msg), Timeout);
	HAL_UART_Transmit(&huart1, temp.temperatuur_tx, sizeof(temp.temperatuur_tx), Timeout);
	HAL_UART_Transmit(&huart1, enter, sizeof(enter), Timeout);
	/* deactivate chip select */
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, SET);
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_SPI1_Init();
  /* USER CODE BEGIN 2 */
  HAL_UART_Transmit(&huart1, greetings, sizeof(greetings), Timeout);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  GetMSBtempAndLSBTempFromTC72();
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  HAL_Delay(Delay);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : CS_Pin */
  GPIO_InitStruct.Pin = CS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(CS_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

