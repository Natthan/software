#ifndef LINKLIST_H
#define LINKLIST_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "termios.h"
#include <stddef.h>
#include <ctype.h>
#include "CalFunc.h"

#define N 26
#define M 10

typedef struct TrieNode TrieNode;
typedef void(*command_function)(char *, char*);
void(*fp)(char *, char*);

TrieNode *ptr;

struct TrieNode {
    char data;
    TrieNode* children[N];
    int is_leaf;
    command_function function;
};


TrieNode* Build_Trienode(char data);
TrieNode* Build_Command_Trie(TrieNode* root);
TrieNode* Insert_Command_Trie(TrieNode* root, char* word, command_function functie);

int Search_Command_Trie(TrieNode* root, char* word, char* first_arg, char* sec_arg);

void Print_Builded_Trie(TrieNode* root);
void Free_Trienode(TrieNode* node);

static void set_stdin_to_raw(void);
static void restore_stdin(void);
static struct termios term;

int ch = 0;
char command_buf[20] = {0};
int i = 0;

#endif