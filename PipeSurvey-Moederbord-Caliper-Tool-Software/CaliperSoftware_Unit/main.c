#include <stdio.h>

#include "CalFunc.h"
#include "CalFunc.c"

#include "LinkList.h"
#include "LinkList.c"

TrieNode *Build_Command_Trie(TrieNode* root){
	root = Insert_Command_Trie(root, (char*)"s", &SetSample);
    root = Insert_Command_Trie(root, (char*)"cp", &PowerCalib);
    root = Insert_Command_Trie(root, (char*)"d", &download);
    root = Insert_Command_Trie(root, (char*)"i", &PowerMem);
    root = Insert_Command_Trie(root, (char*)"o", &PowerOdo);
    root = Insert_Command_Trie(root, (char*)"pr", &PowerPress);
    root = Insert_Command_Trie(root, (char*)"pow", &PowerSensor);
    root = Insert_Command_Trie(root, (char*)"t", &PowerTemp);
    root = Insert_Command_Trie(root, (char*)"r", &ReadData);
    root = Insert_Command_Trie(root, (char*)"cb", &Calibrate);
    root = Insert_Command_Trie(root, (char*)"cm", &CommunicationMode);
    root = Insert_Command_Trie(root, (char*)"rtc", &RTC_init);
    root = Insert_Command_Trie(root, (char*)"on", &AllSensorsOn);
    root = Insert_Command_Trie(root, (char*)"help", &DisplayInfo);
    //print_trie(root);
    //printf("\n");
    return root;
}

TrieNode *CommandTrie;

void GetCommand(){
    char *command, *argument, *second_arg;
    while((ch = getchar()) != '\n'){
        command_buf[i++] = tolower(ch);            
    }
    //printf("Entered command: %s\r\n", command_buf);

    command = strtok(command_buf, " "); // extract command from input s
    argument = strtok(NULL, " "); // extract argument from input s
    second_arg = strtok(NULL, " "); // extract second argument from input s

    Search_Command_Trie(CommandTrie, command_buf, argument, second_arg);
    memset(command_buf, 0, sizeof(command_buf));
    i=0;
}

int main(){
    // printf("LinkList say: Welkom to linkedlist.c\r\n");
    //TrieNode root = {'\0'};
    CommandTrie = Build_Trienode('\0');

    Build_Command_Trie(CommandTrie);
    //Print_Builded_Trie(CommandTrie);

    printf("\r\nType help for instructions!");

    while(1){
        printf("\r\nEnter command-> ");
        GetCommand();
    }
    return 0;
}

