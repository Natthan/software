#ifndef CALFUNC_H
#define CALFUNC_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "RTC.h"
#include "RTC.c"

#include "SensorsStats.h"

void DisplayInfo(char *first_a, char *sec);

void SetSample(char *first_a, char *sec_a);

void SetCalibSensors(int first_sensor, int status);
void PowerCalib(char *first_a, char *sec_a);

void SetOdoSensors(int number, int status);
void PowerOdo(char *first_a, char *sec_a);

void SetPressSensors(int number, int status);
void PowerPress(char *first_a, char *sec_a);

void Calibrate(char *first_a, char *sec_a);

void AllSensorsOn(char* first_a, char *sec_a);

int ch_calfunc = 0;
char GetLengthCaliperArmBuf[5] = {0};
int i_calfunc = 0;

#endif