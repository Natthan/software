#include <iostream>
#include <cstring>
using namespace std;

#include "UART_interface.h"
#include "Functions.h"

int main(){
       
    // Default communication mode if connect to pc
    show.commu();
    cout << "Ready when you are." << endl;
    while(1){
        char input[100] = {'\0'};
        char input2[100] = {'\0'};

        // Show status
        call.DisplayStatus();

        // Get command
        cout << ">> ";
        cin.getline(input, sizeof(input));

        // Split command into command and argument and second argument
        call.split(input);
    }
    return 0;
}

/*
*  extract second argument from input
*/
void UART_interface::execute_second_arg(char t[]){
    cout << "Second argument: ";
    cout << t << endl;
}

/*
* extract argument from input
*/
void UART_interface::execute_arg(char a[]){
    cout << "Argument: ";
    cout << a << endl;
}

/*
* extract command form input
*/
void UART_interface::execute_command(char c[], char a[], char t[]){
    cout << "Command: ";
    cout << c << endl;
    execute_arg(a);
    //execute_second_arg(t);
}

/*
* split input into command, argument and second argument
*/
bool UART_interface::split(char s[]){
    bool reconized, arg, sec_arg; // true of false variables

    command = strtok(s, " "); // extract command from input s
    //cout << "Eerste input: ";
    //cout << command << endl;

    reconized = call.CheckFunction(command); // check if command is reconized

    if(reconized == true){
        // command is reconized
        if(command != NULL && argument == NULL){
            if(strcasecmp(command, "download") == 0){
                // function download is called, without argument
                return call.download();
            }

            else if(strcasecmp(command, "communication") == 0){
                // function communication is called, without argument
                return call.commu();
            }

            else if(strcasecmp(command, "activation") == 0){
                // function activiation is called, without argument
                return call.acti();
            }
            //call.ExecuteArg(command, argument);
        }
        argument = strtok(NULL, " "); // extract argument from input s
        second_arg = strtok(NULL, " "); // extract second argument from input s
        if(argument != NULL && second_arg == NULL){
            /* 
             * argument is given but second argument is not 
             * call function without second argument
             */
            //call.ExecuteArg(command, argument);
            return true;
        }
        else if(argument != NULL && second_arg != NULL){
            /* 
            *argument is given and also second argument 
            * call function with second argument
            */
            call.ExecuteSecArg(command, argument, second_arg);
            return true;
        }
        else if(argument == NULL){
            /*
            * argument is not given
            * call function without argument
            * */
            if(strcasecmp(command, "hoek")== 0){
                //cout << "Possible arguments are: group<N>, on or off, N=1-7" << endl;
                return true;
            }

            call.ExecuteArg(command, argument);
        }
    }

    else{
        //cout << "Command not reconized" << endl;
		//cout << "Possible commands are: sample, kalibreer, download, voeding, hoek, odo, read, gyroacc, druk " << endl << endl;
        return false;
    }
}

/*
 * Display status of the sensors 
 */
void UART_interface::DisplayStatus(){
    cout << endl;
    cout << "Hoek group 1: " << set.group1; cout << " | Hoek group 2: " << set.group2; cout << " | Hoek group 3: " << set.group3; cout << " | Hoek group 4: " << set.group4 << endl; 
    cout << "Hoek group 5: " << set.group5; cout << " | Hoek group 6: " << set.group6; cout << " | Hoek group 7: " << set.group7 << endl;
    cout << endl;
    cout << "Odometer status: " << set.odo_status; cout << " | Druksensor status: " << set.druk_status; cout << " | GyroAcc status: " << set.gyroacc_status << endl; 
    cout << endl;
    cout << "Activation mode: " << set.activate_status; cout << " | Communication mode:: " << set.commu_status << endl;
    cout << "5V status: " << set.Boost; cout << " | 3v3 status: " << set.Buck << endl;
    cout << endl;
    cout << "Sample frequency: " << set.samplefreq << endl;
    cout << "Possible commands are: sample, kalibreer, download, voeding, hoek, odo, read, gyroacc, druk " << endl << endl;


}