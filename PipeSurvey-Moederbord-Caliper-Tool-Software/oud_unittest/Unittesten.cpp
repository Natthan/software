#include <stdio.h>
#include "Functions.h"
#include "UART_interface.h"
#include <windows.h>

static int tests = 0;
static int fails = 0;

#define TEST(condition, ...) \
    tests++;\
    if (!(condition))\
    {\
        fails++;\
        printf("Error: ");\
        printf(__VA_ARGS__);\
        printf("\n");\
    }

#define PRINT_TEST_REPORT\
    printf("%d tests performed: %d succeded, %d failed.\n", tests, tests - fails, fails);


/* Test 1: Check if command is being reconized */
const int max_size_test1 = 15;
char test1[max_size_test1][max_size_test1] = {"sample","jh","lkjdslf","kalibreer", "download", "voeding", "hoek", "odo", "read", "gyroacc", "druk", "sdf323", "2323", "activation","communication"};
void TestCommand(){
    Functions call;
    printf("\t\t\tTest 1 begin: Check if command is being reconized\r\n");
    for(int i=0; i<max_size_test1; i++){
        printf("Command: %s\r\n", test1[i]);
        TEST(call.CheckFunction(test1[i]), "Command is not being reconized\r\n");
    }
    printf("\t\t\t\t\tTest 1 end\r\n\r\n");
}

/* Test 2: Check is argument of a command is being reconized */
char test2[4][4] = {"on","off","sdj"};
void TestArgument(){
    Functions call;
    printf("\t\tTest 2 begin: Check if an argument of a command is being reconized\r\n");

    printf("Test argugment for function odo\r\n");
    for(int i=0; i<3; i++){
        printf("Argument: %s\r\n", test2[i]);
        TEST(call.CheckArg("odo", test2[i]), "Argument is not being reconized\r\n");
    }

    printf("Test argugment for function GyroAcc\r\n");
    for(int i=0; i<3; i++){
        printf("Argument: %s\r\n", test2[i]);
        TEST(call.CheckArg("GyroAcc", test2[i]), "Argument is not being reconized\r\n");
    }

    printf("Test argugment for function druk\r\n");
    for(int i=0; i<3; i++){
        printf("Argument: %s\r\n", test2[i]);
        TEST(call.CheckArg("druk", test2[i]), "Argument is not being reconized\r\n");
    }

    printf("Test argugment for function activation\r\n");
    for(int i=0; i<3; i++){
        printf("Argument: %s\r\n", test2[i]);
        TEST(call.CheckArg("activation", test2[i]), "Argument is not being reconized\r\n");
    }

    printf("Test argugment for function communication\r\n");
    for(int i=0; i<3; i++){
        printf("Argument: %s\r\n", test2[i]);
        TEST(call.CheckArg("communication", test2[i]), "Argument is not being reconized\r\n");
    }

    printf("Test argugment for function voeding\r\n");
    for(int i=0; i<3; i++){
        printf("Argument: %s\r\n", test2[i]);
        TEST(call.CheckArg("voeding", test2[i]), "Argument is not being reconized\r\n");
    }
    printf("\t\t\t\t\tTest 2 end\r\n\r\n");
}

/* Test 3: Check if input for the function sample is valid */
const int max_size_test3 = 5;
char test3[max_size_test3][max_size_test3] = {"1","2","500","2000","2500"};
void TestSampleFunction(){
    
    printf("\t\t\tTest 3 begin: Check if input sample is within range\r\n");
    for(int i=0; i<max_size_test3; i++){
        printf("Input sample freq: %s\r\n", test3[i]);
        TEST(call.CheckArg("sample", test3[i]), "Is not between 4 Hz and 2000 Hz\r\n");
    }
    printf("\t\t\t\t\tTest 3 end\r\n\r\n");
}

/* Test 4: Check if argument for function read is valid */
char test4[10][10] = {"voltage", "temp", "blue"};
void TestReadFunction(){
    printf("\t\t\tTest 4 begin: Check if input for rad function is valid\r\n");
    for(int i=0; i<3; i++){
        printf("Argument: %s\r\n", test4[i]);
        TEST(call.CheckArg("read", test4[i]), "Argument is not being reconized\r\n");
    }
    printf("\t\t\t\t\tTest 4 end\r\n\r\n");
}



int main(void)
{
    while(1){
        TestCommand();
        TestArgument();
        TestSampleFunction();
        TestReadFunction();
        PRINT_TEST_REPORT
        Sleep(100000);
    }
    //return 0;
}