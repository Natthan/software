#include <iostream>
#include <cstring>
using namespace std;

#include "UART_interface.h"
#include "Functions.h"

int main(){
    UART_interface call;
    Functions show;        
    // Default communication mode if connect to pc
    show.commu();
    cout << "Ready when you are." << endl;
    while(1){
        char input[100] = {'\0'};
        char input2[100] = {'\0'};

        // Show status
        call.DisplayStatus();

        // Get command
        cout << ">> ";
        cin.getline(input, sizeof(input));

        // Split command into command and argument and second argument
        call.split(input);

    }
}