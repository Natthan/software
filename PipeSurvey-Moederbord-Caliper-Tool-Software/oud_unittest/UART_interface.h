#ifndef UART_interface_H_
#define UART_interface_H_

#include <iostream>
#include <cstring>
using namespace std;

#include "Functions.h"

// Hulp variable
int i=0;

// split function variable
char *command, *argument, *second_arg;

// // Define functions
// void split(char s[]); // split input into command, argument and second argument
// void execute_command(char s[], char a[], char t[]); // excecute the command
// void execute_arg(char s[]); // execute command with argument
// void execute_second_arg(char t[]); // execute command with argument and second argument

class UART_interface: public status{
public:
    void split(char s[]); // split input into command, argument and second argument
    void execute_command(char s[], char a[], char t[]); // excecute the command
    void execute_arg(char s[]); // execute command with argument
    void execute_second_arg(char t[]); // execute command with argument and second argument
    void DisplayStatus(); // display status on uart
};

/*
*  extract second argument from input
*/
void UART_interface::execute_second_arg(char t[]){
    cout << "Second argument: ";
    cout << t << endl;
}

/*
* extract argument from input
*/
void UART_interface::execute_arg(char a[]){
    cout << "Argument: ";
    cout << a << endl;
}

/*
* extract command form input
*/
void UART_interface::execute_command(char c[], char a[], char t[]){
    cout << "Command: ";
    cout << c << endl;
    execute_arg(a);
    //execute_second_arg(t);
}

/*
* split input into command, argument and second argument
*/
void UART_interface::split(char s[]){
    bool reconized, arg, sec_arg; // true of false variables

    command = strtok(s, " "); // extract command from input s
    //cout << "Eerste input: ";
    //cout << command << endl;

    reconized = call.CheckFunction(command); // check if command is reconized

    if(reconized == true){
        // command is reconized
        if(command != NULL && argument == NULL){
            if(strcasecmp(command, "download") == 0){
                // function download is called, without argument
                call.download();
                return;
            }

            else if(strcasecmp(command, "communication") == 0){
                // function communication is called, without argument
                call.commu();
                return;
            }

            else if(strcasecmp(command, "activation") == 0){
                // function activiation is called, without argument
                call.acti();
                return;
            }
            //call.ExecuteArg(command, argument);
        }
        argument = strtok(NULL, " "); // extract argument from input s
        second_arg = strtok(NULL, " "); // extract second argument from input s
        if(argument != NULL && second_arg == NULL){
            /* 
             * argument is given but second argument is not 
             * call function without second argument
             */
            call.ExecuteArg(command, argument);
        }
        else if(argument != NULL && second_arg != NULL){
            /* 
            *argument is given and also second argument 
            * call function with second argument
            */
            call.ExecuteSecArg(command, argument, second_arg);
        }
        else if(argument == NULL){
            /*
            * argument is not given
            * call function without argument
            * */
            if(strcasecmp(command, "hoek")== 0){
                cout << "Possible arguments are: group<N>, on or off, N=1-7" << endl;
                return;
            }

            call.ExecuteArg(command, argument);
        }
    }

    else{
        cout << "Command not reconized" << endl;
		cout << "Possible commands are: sample, kalibreer, download, voeding, hoek, odo, read, gyroacc, druk, activation, communication " << endl << endl;
    }
}

/*
 * Display status of the sensors 
 */
void UART_interface::DisplayStatus(){
    cout << endl;
    cout << "Hoek group 1: " << set.group1; cout << " | Hoek group 2: " << set.group2; cout << " | Hoek group 3: " << set.group3; cout << " | Hoek group 4: " << set.group4; 
    cout << " | Hoek group 5: " << set.group5; cout << " | Hoek group 6: " << set.group6; cout << " | Hoek group 7: " << set.group7 << endl;
    cout << endl;
    cout << "Odometer status: " << set.odo_status; cout << " | Druksensor status: " << set.druk_status; cout << " | GyroAcc status: " << set.gyroacc_status << endl; 
    cout << endl;
    cout << "Activation mode: " << set.activate_status; cout << " | Communication mode: " << set.commu_status << endl;
    cout << endl;
    cout << "5V status: " << set.Boost; cout << " | 3v3 status: " << set.Buck << endl;
    cout << endl;
    cout << "Sample frequency: " << set.samplefreq << endl;
    cout << "Possible commands are: sample, kalibreer, download, voeding, hoek, odo, read, gyroacc, druk, activaction, communication " << endl << endl;
}

#endif /* UART_inteface_H_ */
