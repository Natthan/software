#include <iostream>
#include <cstring>
using namespace std;

#include "TestInterfaceUart.h" 
// #include "TestFunction.h" 

int main(){
    cout << "Ready when u is" << endl;
	call.commu();
    while(1){
        char input[100] = {'\0'};
        char input2[100] = {'\0'};

        // Show status
        go.DisplayStatus();

        // Get command
        cout << ">> ";
        cin.getline(input, sizeof(input));

        // Split command into command and argument and second argument
        go.split(input);

    }
    return 0;
}

// void uart::split(char *i){
//     cout << "Split function is called" << endl;
// }

/*
*  extract second argument from input
*/
void uart::execute_second_arg(char t[]){
    cout << "Second argument: ";
    cout << t << endl;
}

/*
* extract argument from input
*/
void uart::execute_arg(char a[]){
    cout << "Argument: ";
    cout << a << endl;
}

/*
* extract command form input
*/
void uart::execute_command(char c[], char a[], char t[]){
    cout << "Command: ";
    cout << c << endl;
    execute_arg(a);
    //execute_second_arg(t);
}

/*
* split input into command, argument and second argument
*/
void uart::split(char s[]){
    bool reconized, arg, sec_arg; // true of false variables

    command = strtok(s, " "); // extract command from input s
    //cout << "Eerste input: ";
    //cout << command << endl;

    reconized = call.CheckFunction(command); // check if command is reconized

    if(reconized == true){
        // command is reconized
        if(command != NULL && argument == NULL){
            if(strcasecmp(command, "download") == 0){
                // function download is called, without argument
                call.download();
                return;
            }

            else if(strcasecmp(command, "communication") == 0){
                // function communication is called, without argument
                call.commu();
                return;
            }

            else if(strcasecmp(command, "activation") == 0){
                // function activiation is called, without argument
                call.acti();
                return;
            }
            //call.ExecuteArg(command, argument);
        }
        argument = strtok(NULL, " "); // extract argument from input s
        second_arg = strtok(NULL, " "); // extract second argument from input s
        if(argument != NULL && second_arg == NULL){
            /* 
             * argument is given but second argument is not 
             * call function without second argument
             */
            call.ExecuteArg(command, argument);
        }
        else if(argument != NULL && second_arg != NULL){
            /* 
            *argument is given and also second argument 
            * call function with second argument
            */
            call.ExecuteSecArg(command, argument, second_arg);
        }
        else if(argument == NULL){
            /*
            * argument is not given
            * call function without argument
            * */
            if(strcasecmp(command, "hoek")== 0){
                cout << "Possible arguments are: group<N>, on or off, N=1-7" << endl;
                return;
            }

            call.ExecuteArg(command, argument);
        }
    }

    else{
        cout << "Command not reconized" << endl;
		cout << "Possible commands are: sample, kalibreer, download, voeding, hoek, odo, read, gyroacc, druk " << endl << endl;
    }
}

/*
 * Display status of the sensors 
 */
void uart::DisplayStatus(){
    cout << endl;
    cout << "Hoek group 1: " << set.group1; cout << " | Hoek group 2: " << set.group2; cout << " | Hoek group 3: " << set.group3; cout << " | Hoek group 4: " << set.group4 << endl; 
    cout << "Hoek group 5: " << set.group5; cout << " | Hoek group 6: " << set.group6; cout << " | Hoek group 7: " << set.group7 << endl;
    cout << endl;
    cout << "Odometer status: " << set.odo_status; cout << " | Druksensor status: " << set.druk_status; cout << " | GyroAcc status: " << set.gyroacc_status << endl; 
    cout << endl;
    cout << "Activation mode: " << set.activate_status; cout << " | Communication mode:: " << set.commu_status << endl;
    cout << "5V status: " << set.Boost; cout << " | 3v3 status: " << set.Buck << endl;
    cout << endl;
    cout << "Sample frequency: " << set.samplefreq << endl;
    cout << "Possible commands are: sample, kalibreer, download, voeding, hoek, odo, read, gyroacc, druk " << endl << endl;
}

/*
* check if function exist, return value is true or false
* return value true or false
*/
bool Functions::CheckFunction(char *i){
	if(i == NULL){
		cout << "No command given" << endl;
		return false;
	}

	if(strcasecmp(i, "hoek") == 0){
		return true;
	}
	
	else if(strcasecmp(i, "odo") == 0){
		return true;
	}

	else if(strcasecmp(i, "gyroAcc") == 0){
		return true;
	}

	else if(strcasecmp(i, "druk") == 0){
		return true;
	}

	else if(strcasecmp(i, "read") == 0){
		return true;
	}

	else if(strcasecmp(i, "communication") == 0){
		return true;
	}

	else if(strcasecmp(i, "activation") == 0){
		return true;
	}

	else if(strcasecmp(i, "download") == 0){
		return true;
	}

	else if(strcasecmp(i, "kalibreer") == 0){
		return true;
	}

	else if(strcasecmp(i, "sample") == 0){
		return true;
	}

	else if(strcasecmp(i, "voeding") == 0){
		return true;
	}

	else{
		// no command found
		return false;
	}
}

/*
 * Check if argument exist
 * Return value true of false
*/
bool Functions::CheckArg(char *i, char *a){
    if(a == 0){ 
        //argument = "None";
        cout << "No argument given" << endl;
		cout << "Possible commands are: on, off, between 40 Hz and 2000 Hz, etc" << endl;
        return false;
    }
	
	else if(strcasecmp(i, "odo") == 0){
		if(strcasecmp(a, "on") == 0 || strcasecmp(a, "off") == 0){
			return true;
		}
		else{
			return false;
		}
	}

	else if(strcasecmp(i, "gyroAcc") == 0){
		if(strcasecmp(a, "on") == 0 || strcasecmp(a, "off") == 0){
			return true;
		}
		else{
			return false;
		}
	}

	else if(strcasecmp(i, "druk") == 0){
		if(strcasecmp(a, "on") == 0 || strcasecmp(a, "off") == 0){
			return true;
		}
		else{
			return false;
		}
	}

	else if(strcasecmp(i, "read") == 0){
		if(strcasecmp(a, "voltage") == 0 || strcasecmp(a, "temp") == 0){
			return true;
		}
		else{
			return false;
		}
	}

	else if(strcasecmp(i, "communication") == 0){
		if(strcasecmp(a, "on") == 0 || strcasecmp(a, "off") == 0){
			return true;
		}
		else{
			return false;
		}
	}

	else if(strcasecmp(i, "activation") == 0){
		if(strcasecmp(a, "on") == 0 || strcasecmp(a, "off") == 0){
			return true;
		}
		else{
			return false;
		}
	}

	// else if(strcasecmp(i, "download") == 0){
	// 	return true;
	// }

	else if(strcasecmp(i, "kalibreer") == 0){
		if(strcasecmp(a, "on") == 0 || strcasecmp(a, "off") == 0){
			return true;
		}
		else{
			return false;
		}
	}

	else if(strcasecmp(i, "sample") == 0){
		int val;
		val = atoi(a);
		if(val > 4 && val < 2000){
			return true;
		}
		else{
			return false;
		}
	}

	else if(strcasecmp(i, "voeding") == 0){
		return true;
	}

	else{
		cout << "Argument not reconized" << endl;
		cout << "Possible commands are: on, off, between 40 Hz and 2000 Hz, etc" << endl;
		return false;
	}
}

/*
 * Execute command with argument
 */
void Functions::ExecuteArg(char *i, char *a){
    if(a == 0){ 
        //argument = "None";
        cout << "No argument given" << endl;
		cout << "Possible arguments are: on, off, between 40 Hz and 2000 Hz, etc" << endl;
        return;
    }
	
	else if(strcasecmp(i, "odo") == 0){
		call.odo(a);
		return;
	}

	else if(strcasecmp(i, "gyroAcc") == 0){
		call.GyroAcc(a);
		return;
	}

	else if(strcasecmp(i, "druk") == 0){
		call.druk(a);
		return;
	}

	else if(strcasecmp(i, "read") == 0){
		call.read(a);
		return;
	}

	else if(strcasecmp(i, "kalibreer") == 0){
		call.kalibreer(a);
		return;
	}

	else if(strcasecmp(i, "sample") == 0){
		call.sample(a);
		return;
	}

	else if(strcasecmp(i, "voeding") == 0){
		call.voeding(a);
		return;
	}

	else{
		cout << "Argument not reconized" << endl;
		cout << "Possible commands are: on, off, between 40 Hz and 2000 Hz, etc" << endl;
	}
}


/* 
 * Execute command with argument and second arguemnt
 */
void Functions::ExecuteSecArg(char *i, char *a, char *s){
    if(s == 0){ 
        //argument = "None";
        cout << "No second argument given" << endl;
		cout << "Possible second arguments are: group1, group2, group3, group4, group5, group6, group7, group8, \n group9, group10, group11, group12, group13;" << endl;
        return;
    }	
	
	if((strcasecmp(a, "group1") == 0) && strcasecmp(s, "on") == 0){
		for(int i=1; i<=10; i++){
			cout << "Hoeksensor:" << i << " on" << endl;
		}
		set.group1 = "on";
		return;
	}


	if((strcasecmp(a, "group1") == 0) && strcasecmp(s, "off") == 0){
		for(int i=1; i<=10; i++){
			cout << "Hoeksensor:" << i << " off" << endl;
			
		}
		set.group1 = "off";
		return;
	}

	if((strcasecmp(a, "group2") == 0) && strcasecmp(s, "on") == 0){
		for(int i=11; i<=20; i++){
			cout << "Hoeksensor:" << i << " on" << endl;
		}
		set.group2 = "on";
		return;
	}


	if((strcasecmp(a, "group2") == 0) && strcasecmp(s, "off") == 0){
		for(int i=11; i<=20; i++){
			cout << "Hoeksensor:" << i << " off" << endl;
			
		}
		set.group2 = "off";
		return;
	}

	if((strcasecmp(a, "group3") == 0) && strcasecmp(s, "on") == 0){
		for(int i=21; i<=30; i++){
			cout << "Hoeksensor:" << i << " on" << endl;
		}
		set.group3 = "on";
		return;
	}


	if((strcasecmp(a, "group3") == 0) && strcasecmp(s, "off") == 0){
		for(int i=21; i<=30; i++){
			cout << "Hoeksensor:" << i << " off" << endl;
			
		}
		set.group3 = "off";
		return;
	}

	if((strcasecmp(a, "group4") == 0) && strcasecmp(s, "on") == 0){
		for(int i=31; i<=40; i++){
			cout << "Hoeksensor:" << i << " on" << endl;
		}
		set.group4 = "on";
		return;
	}


	if((strcasecmp(a, "group4") == 0) && strcasecmp(s, "off") == 0){
		for(int i=31; i<=40; i++){
			cout << "Hoeksensor:" << i << " off" << endl;
			
		}
		set.group4 = "off";
		return;
	}

	if((strcasecmp(a, "group5") == 0) && strcasecmp(s, "on") == 0){
		for(int i=41; i<=50; i++){
			cout << "Hoeksensor:" << i << " on" << endl;
		}
		set.group5 = "on";
		return;
	}


	if((strcasecmp(a, "group5") == 0) && strcasecmp(s, "off") == 0){
		for(int i=41; i<=50; i++){
			cout << "Hoeksensor:" << i << " off" << endl;
			
		}
		set.group5 = "off";
		return;
	}

	if((strcasecmp(a, "group6") == 0) && strcasecmp(s, "on") == 0){
		for(int i=51; i<=60; i++){
			cout << "Hoeksensor:" << i << " on" << endl;
		}
		set.group6 = "on";
		return;
	}


	if((strcasecmp(a, "group6") == 0) && strcasecmp(s, "off") == 0){
		for(int i=51; i<=60; i++){
			cout << "Hoeksensor:" << i << " off" << endl;
			
		}
		set.group6 = "off";
		return;
	}

	if((strcasecmp(a, "group7") == 0) && strcasecmp(s, "on") == 0){
		for(int i=61; i<=64; i++){
			cout << "Hoeksensor:" << i << " on" << endl;
		}
		set.group7 = "on";
		return;
	}


	if((strcasecmp(a, "group7") == 0) && strcasecmp(s, "off") == 0){
		for(int i=61; i<=64; i++){
			cout << "Hoeksensor:" << i << " off" << endl;
			
		}
		set.group7 = "off";
		return;
	}

	else{
		cout << "Argument not reconized" << endl;
		cout << "Possible commands are: group<N>, on or off, N=1-13" << endl;
	}
}

/*
 * Function to sample frequency rate
 */
void Functions::sample(char* arg){
	if(arg == NULL) {
		cout << "Dit not get argument" << endl;
		return;
	}
	while(arg != NULL){
		int argument = atoi(arg);
		if(argument >= 4 && argument <= 2000){
			cout << "Sample frequentcy set to: " << arg << " Hz" << endl;
			set.samplefreq = argument;
			return;
		}
		else{
			cout << "Unknown argument: "<< arg << endl;
			cout << "Possible argument is between 4 Hz and 2000 Hz" << endl;
			return;
		}
	}
}

/*
 * Function calibrate AS5162
 */
void Functions::kalibreer(char* arg){
	// if(arg == NULL) {
	// 	cout << "Did not get argument" << endl;
	// }
	while(arg != NULL){
		if(strcmp(arg, "on") == 0){
		    cout << "Calibrate compleet" << endl;
		    return;
		}
		if(strcmp(arg, "off") == 0){
		    cout << "Calibrate set on default" << endl;
		    return;
		}
		else{
			cout << "Unknown argument: "<< arg << endl;
			cout << "Possible arguments are: on, off" << endl;
			return;
		}
	}
}

/*
 * Function download saved data
 */
void Functions::download(){
	cout << "Download started" << endl;
}

/*
 * Function to set system to communication mode. 
 * All sensors are off
 * Download at high speed 50 Mbps, because that is the SPI top clock speed.
 */
void Functions::commu(){
	cout << "System set in commucation mode" << endl;
	set.activate_status = "off";
	set.commu_status = "on";
	set.group1 = "off"; 
	set.group2 = "off"; 
	set.group3 = "off"; 
	set.group4 = "off"; 
	set.group5 = "off"; 
	set.group6 = "off"; 
	set.group7 = "off";
	set.samplefreq = 2000;
	set.odo_status = "off";
	set.druk_status = "off";
	set.gyroacc_status = "off";
	set.Boost = "off";
	set.Buck = "off";
}

/*
 * Function to set system in activation mode.
 * All sensors are on.
 * Reading data are being saved.
 */
void Functions::acti(){
	cout << "System set in activation modue" << endl;
	set.activate_status = "on";
	set.commu_status = "off";
	set.group1 = "on"; 
	set.group2 = "on"; 
	set.group3 = "on"; 
	set.group4 = "on"; 
	set.group5 = "on"; 
	set.group6 = "on"; 
	set.group7 = "on";
	set.samplefreq = 2000;
	set.odo_status = "on";
	set.druk_status = "on";
	set.gyroacc_status = "on";
	set.Boost = "on";
	set.Buck = "on";
}

/*
 * Function to turn odometers on or off
 */
void Functions::odo(char* arg){
	// if(arg == NULL) {
	// 	cout << "Did not get argument" << endl;
	// }
	while(arg != NULL){
		if(strcmp(arg, "on") == 0){
		    cout << "Odometers on" << endl;
			set.odo_status = "on";
		    return;
		}
		if(strcmp(arg, "off") == 0){
		    cout << "Odometers off" << endl;
			set.odo_status = "off";
		    return;
		}
		else{
			cout << "Unknown argument: "<< arg << endl;
			cout << "Possible arguments are: on, off" << endl;
			return;
		}
	}
}

/*
 * Function to turn pressure sensors on or off
 */
void Functions::druk(char* arg){
	// if(arg == NULL) {
	// 	cout << "Did not get argument" << endl;
	// }
	while(arg != NULL){
		if(strcmp(arg, "on") == 0){
		    cout << "Druksensoren on" << endl;
			set.druk_status = "on";
		    return;
		}
		if(strcmp(arg, "off") == 0){
		    cout << "Druksensoren off" << endl;
			set.druk_status = "off";
		    return;
		}
		else{
			cout << "Unknown argument: "<< arg << endl;
			cout << "Possible arguments are: on, off" << endl;
			return;
		}
	}
}

/*
 * Function to turn Gyroscopes and Accelerometers on or off
 */
void Functions::GyroAcc(char* arg){
	// if(arg == NULL) {
	// 	cout << "Dit not get argument" << endl;
	// }
	while(arg != NULL){
		if(strcmp(arg, "on") == 0){
		    cout << "GyroAcc on" << endl;
			set.gyroacc_status = "on";
		    return;
		}
		if(strcmp(arg, "off") == 0){
		    cout << "GyroAcc off" << endl;
			set.gyroacc_status = "off";
		    return;
		}
		else{
			cout << "Unknown argument: "<< arg << endl;
			cout << "Possible arguments are: on, off" << endl;
			return;
		}
	}
}

/*
 * Function to turn power supply off.
 * All sensors are turn off.
 * MCU remain in low power mode.
 */
void Functions::voeding(char* arg){
	// if(arg == NULL) {
	// 	cout << "Dit not get argument" << endl;
	// }
	while(arg != NULL){
		if(strcmp(arg, "on") == 0){
		    cout << "All sensors are on" << endl;
			set.Boost = "on";
			set.Buck = "on";
		    return;
		}
		if(strcmp(arg, "off") == 0){
		    cout << "All sensors are off" << endl;
			set.Boost = "off";
			set.Buck = "off";
		    return;
		}
		else{
			cout << "Unknown argument: "<< arg << endl;
			cout << "Possible arguments are: on, off" << endl;
			return;
		}
	}
}

/*
 * Function to turn read to voltage of the battery
 * Function to read temperature at three differentes positions.
 * Position 1: Cable at the front of the module .
 * Position 2: Cable at the back of the module.
 * Position 3: On-board. 
 */
void Functions::read(char* arg){
	// if(arg == NULL) {
	// 	cout << "Dit not get argument" << endl;
	// }
	while(arg != NULL){
		if(strcmp(arg, "voltage") == 0){
		    cout << "Voltage of the battery is 5 V" << endl;
		    return;
		}
		if(strcmp(arg, "temp") == 0){
		cout << "Temp position 1: " << "25 C" << endl;
		cout << "Temp position 2: " << "26 C" << endl;
		cout << "Temp position 3: " << "30 C" << endl;
		    return;
		}
		else{
			cout << "Unknown argument: "<< arg << endl;
			cout << "Possible arguments are: voltage, temp" << endl;
			return;
		}
	}
}




