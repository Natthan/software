#ifndef TestFunction_H_
#define TestFunction_H_

// #include "TestFunction.h" 

#include <iostream>
#include <cstring>
#include <cstdio>
#include <cstdlib>
using namespace std;

class status{
	public:
	string group1, group2, group3, group4, group5, group6, group7;
	string commu_status, activate_status, druk_status, odo_status, Boost, Buck, gyroacc_status;
	int samplefreq;
};

status set;

class Functions: public status{
	public:	
	//void hoek(char* arg, char *second_arg);
	
	bool CheckFunction(char i[]); // check if function exist	
	bool CheckArg(char i[], char a[]);	// check is argument exist

	void ExecuteArg(char i[], char a[]); // execute functions wihtout second argument
	void ExecuteSecArg(char i[], char a[], char s[]); // execute functions with second argument

	void sample(char* arg); // function to set sample rate
	void kalibreer(char* arg); // function to kalibrate
	void download(); // function to download data
	void read(char* arg); // function to read value
	void voeding(char* arg); // function to control the power supply of the sensors
	void commu(); // function to set the system to communication mode
	void acti(); // function to set the system to activation mode
	void odo(char* arg); // function to control odometers
	void GyroAcc(char* arg); // function to control gyroscopes and accelerometers
	void druk(char* arg); // function to control druksensors
};

Functions call; 

#endif /* TestFunction_H_ */