#ifndef UART_interface_H_
#define UART_interface_H_

#include <iostream>
#include <cstring>
#include <cstdio>
#include <cstdlib>
using namespace std;

class status{
	public:
	string group1, group2, group3, group4, group5, group6, group7;
	string commu_status, activate_status, druk_status, odo_status, Boost, Buck, gyroacc_status;
	int samplefreq;
};

status set;

class Functions: public status{
	public:	
	//void hoek(char* arg, char *second_arg);
	
	bool CheckFunction(char i[]); // check if function exist	
	bool CheckArg(char i[], char a[]);	// check is argument exist

	void ExecuteArg(char i[], char a[]); // execute functions wihtout second argument
	void ExecuteSecArg(char i[], char a[], char s[]); // execute functions with second argument

	void sample(char* arg); // function to set sample rate
	void kalibreer(char* arg); // function to kalibrate
	void download(); // function to download data
	void read(char* arg); // function to read value
	void voeding(char* arg); // function to control the power supply of the sensors
	void commu(); // function to set the system to communication mode
	void acti(); // function to set the system to activation mode
	void odo(char* arg); // function to control odometers
	void GyroAcc(char* arg); // function to control gyroscopes and accelerometers
	void druk(char* arg); // function to control druksensors
	bool RTC(char *arg); // function to calibrate time
};

Functions call; 

class uart: public Functions{
    public:
        void split(char s[]); // split input into command, argument and second argument
        void execute_command(char s[], char a[], char t[]); // excecute the command
        void execute_arg(char s[]); // execute command with argument
        void execute_second_arg(char t[]); // execute command with argument and second argument
        void DisplayStatus(); // display status on uart
};

uart go;

// Hulp variable
int i=0;

// split function variable
char *command, *argument, *second_arg;

// // Define functions
// void split(char s[]); // split input into command, argument and second argument
// void execute_command(char s[], char a[], char t[]); // excecute the command
// void execute_arg(char s[]); // execute command with argument
// void execute_second_arg(char t[]); // execute command with argument and second argument



// class UART_interface{
// public:
//     void split(char s[]); // split input into command, argument and second argument
//     void execute_command(char s[], char a[], char t[]); // excecute the command
//     void execute_arg(char s[]); // execute command with argument
//     void execute_second_arg(char t[]); // execute command with argument and second argument
//     void DisplayStatus(); // display status on uart
// };

// /*
// *  extract second argument from input
// */
// void UART_interface::execute_second_arg(char t[]){
//     cout << "Second argument: ";
//     cout << t << endl;
// }

// /*
// * extract argument from input
// */
// void UART_interface::execute_arg(char a[]){
//     cout << "Argument: ";
//     cout << a << endl;
// }

// /*
// * extract command form input
// */
// void UART_interface::execute_command(char c[], char a[], char t[]){
//     cout << "Command: ";
//     cout << c << endl;
//     execute_arg(a);
//     //execute_second_arg(t);
// }

// /*
// * split input into command, argument and second argument
// */
// void UART_interface::split(char s[]){
//     bool reconized, arg, sec_arg; // true of false variables

//     command = strtok(s, " "); // extract command from input s
//     //cout << "Eerste input: ";
//     //cout << command << endl;

//     reconized = view.CheckFunction(command); // check if command is reconized

//     if(reconized == true){
//         // command is reconized
//         if(command != NULL && argument == NULL){
//             if(strcasecmp(command, "download") == 0){
//                 // function download is viewed, without argument
//                 view.download();
//                 return;
//             }

//             else if(strcasecmp(command, "communication") == 0){
//                 // function communication is viewed, without argument
//                 view.commu();
//                 return;
//             }

//             else if(strcasecmp(command, "activation") == 0){
//                 // function activiation is viewed, without argument
//                 view.acti();
//                 return;
//             }
//             //view.ExecuteArg(command, argument);
//         }
//         argument = strtok(NULL, " "); // extract argument from input s
//         second_arg = strtok(NULL, " "); // extract second argument from input s
//         if(argument != NULL && second_arg == NULL){
//             /* 
//              * argument is given but second argument is not 
//              * view function without second argument
//              */
//             view.ExecuteArg(command, argument);
//         }
//         else if(argument != NULL && second_arg != NULL){
//             /* 
//             *argument is given and also second argument 
//             * view function with second argument
//             */
//             view.ExecuteSecArg(command, argument, second_arg);
//         }
//         else if(argument == NULL){
//             /*
//             * argument is not given
//             * view function without argument
//             * */
//             if(strcasecmp(command, "hoek")== 0){
//                 cout << "Possible arguments are: group<N>, on or off, N=1-7" << endl;
//                 return;
//             }

//             view.ExecuteArg(command, argument);
//         }
//     }

//     else{
//         cout << "Command not reconized" << endl;
// 		cout << "Possible commands are: sample, kalibreer, download, voeding, hoek, odo, read, gyroacc, druk " << endl << endl;
//     }
// }

// /*
//  * Display status of the sensors 
//  */
// void UART_interface::DisplayStatus(){
//     cout << endl;
//     cout << "Hoek group 1: " << set.group1; cout << " | Hoek group 2: " << set.group2; cout << " | Hoek group 3: " << set.group3; cout << " | Hoek group 4: " << set.group4 << endl; 
//     cout << "Hoek group 5: " << set.group5; cout << " | Hoek group 6: " << set.group6; cout << " | Hoek group 7: " << set.group7 << endl;
//     cout << endl;
//     cout << "Odometer status: " << set.odo_status; cout << " | Druksensor status: " << set.druk_status; cout << " | GyroAcc status: " << set.gyroacc_status << endl; 
//     cout << endl;
//     cout << "Activation mode: " << set.activate_status; cout << " | Communication mode:: " << set.commu_status << endl;
//     cout << "5V status: " << set.Boost; cout << " | 3v3 status: " << set.Buck << endl;
//     cout << endl;
//     cout << "Sample frequency: " << set.samplefreq << endl;
//     cout << "Possible commands are: sample, kalibreer, download, voeding, hoek, odo, read, gyroacc, druk " << endl << endl;
// }



#endif /* TestInterfaceUart_H_ */
