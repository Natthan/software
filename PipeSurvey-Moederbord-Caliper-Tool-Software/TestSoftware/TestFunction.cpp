// #include "TestFunction.h" 
#include "TestInterfaceUart.h"

int main(){
    cout << "This is the main code for TestFunction" << endl;

	// call.commu();
	// call.acti();

    // Functions call;
	// UART_interface go;
	// while(1){
	// 	char input[100] = {'\0'};
    //     char input2[100] = {'\0'};

    //     // Get command
    //     cout << ">> ";
    //     cin.getline(input, sizeof(input));

    //     // Split command into command and argument and second argument
    //     go.split(input);
    //     //
    //     // Execute command with three arguments
    //     //go.execute_command(command, argument, second_arg);
	// 	//
	// 	//call.CheckFunction(command, argument);
	// }

    return 0;
}

// /*
// * check if function exist, return value is true or false
// * return value true or false
// */
// bool Functions::CheckFunction(char *i){
// 	if(i == NULL){
// 		cout << "No command given" << endl;
// 		return false;
// 	}

// 	if(strcasecmp(i, "hoek") == 0){
// 		return true;
// 	}
	
// 	else if(strcasecmp(i, "odo") == 0){
// 		return true;
// 	}

// 	else if(strcasecmp(i, "gyroAcc") == 0){
// 		return true;
// 	}

// 	else if(strcasecmp(i, "druk") == 0){
// 		return true;
// 	}

// 	else if(strcasecmp(i, "read") == 0){
// 		return true;
// 	}

// 	else if(strcasecmp(i, "communication") == 0){
// 		return true;
// 	}

// 	else if(strcasecmp(i, "activation") == 0){
// 		return true;
// 	}

// 	else if(strcasecmp(i, "download") == 0){
// 		return true;
// 	}

// 	else if(strcasecmp(i, "kalibreer") == 0){
// 		return true;
// 	}

// 	else if(strcasecmp(i, "sample") == 0){
// 		return true;
// 	}

// 	else if(strcasecmp(i, "voeding") == 0){
// 		return true;
// 	}

// 	else{
// 		// no command found
// 		return false;
// 	}
// }

// /*
//  * Check if argument exist
//  * Return value true of false
// */
// bool Functions::CheckArg(char *i, char *a){
//     if(a == 0){ 
//         //argument = "None";
//         cout << "No argument given" << endl;
// 		cout << "Possible commands are: on, off, between 40 Hz and 2000 Hz, etc" << endl;
//         return false;
//     }
	
// 	else if(strcasecmp(i, "odo") == 0){
// 		if(strcasecmp(a, "on") == 0 || strcasecmp(a, "off") == 0){
// 			return true;
// 		}
// 		else{
// 			return false;
// 		}
// 	}

// 	else if(strcasecmp(i, "gyroAcc") == 0){
// 		if(strcasecmp(a, "on") == 0 || strcasecmp(a, "off") == 0){
// 			return true;
// 		}
// 		else{
// 			return false;
// 		}
// 	}

// 	else if(strcasecmp(i, "druk") == 0){
// 		if(strcasecmp(a, "on") == 0 || strcasecmp(a, "off") == 0){
// 			return true;
// 		}
// 		else{
// 			return false;
// 		}
// 	}

// 	else if(strcasecmp(i, "read") == 0){
// 		if(strcasecmp(a, "voltage") == 0 || strcasecmp(a, "temp") == 0){
// 			return true;
// 		}
// 		else{
// 			return false;
// 		}
// 	}

// 	else if(strcasecmp(i, "communication") == 0){
// 		if(strcasecmp(a, "on") == 0 || strcasecmp(a, "off") == 0){
// 			return true;
// 		}
// 		else{
// 			return false;
// 		}
// 	}

// 	else if(strcasecmp(i, "activation") == 0){
// 		if(strcasecmp(a, "on") == 0 || strcasecmp(a, "off") == 0){
// 			return true;
// 		}
// 		else{
// 			return false;
// 		}
// 	}

// 	// else if(strcasecmp(i, "download") == 0){
// 	// 	return true;
// 	// }

// 	else if(strcasecmp(i, "kalibreer") == 0){
// 		if(strcasecmp(a, "on") == 0 || strcasecmp(a, "off") == 0){
// 			return true;
// 		}
// 		else{
// 			return false;
// 		}
// 	}

// 	else if(strcasecmp(i, "sample") == 0){
// 		int val;
// 		val = atoi(a);
// 		if(val > 4 && val < 2000){
// 			return true;
// 		}
// 		else{
// 			return false;
// 		}
// 	}

// 	else if(strcasecmp(i, "voeding") == 0){
// 		return true;
// 	}

// 	else{
// 		cout << "Argument not reconized" << endl;
// 		cout << "Possible commands are: on, off, between 40 Hz and 2000 Hz, etc" << endl;
// 		return false;
// 	}
// }

// /*
//  * Execute command with argument
//  */
// void Functions::ExecuteArg(char *i, char *a){
//     if(a == 0){ 
//         //argument = "None";
//         cout << "No argument given" << endl;
// 		cout << "Possible arguments are: on, off, between 40 Hz and 2000 Hz, etc" << endl;
//         return;
//     }
	
// 	else if(strcasecmp(i, "odo") == 0){
// 		call.odo(a);
// 		return;
// 	}

// 	else if(strcasecmp(i, "gyroAcc") == 0){
// 		call.GyroAcc(a);
// 		return;
// 	}

// 	else if(strcasecmp(i, "druk") == 0){
// 		call.druk(a);
// 		return;
// 	}

// 	else if(strcasecmp(i, "read") == 0){
// 		call.read(a);
// 		return;
// 	}

// 	else if(strcasecmp(i, "kalibreer") == 0){
// 		call.kalibreer(a);
// 		return;
// 	}

// 	else if(strcasecmp(i, "sample") == 0){
// 		call.sample(a);
// 		return;
// 	}

// 	else if(strcasecmp(i, "voeding") == 0){
// 		call.voeding(a);
// 		return;
// 	}

// 	else{
// 		cout << "Argument not reconized" << endl;
// 		cout << "Possible commands are: on, off, between 40 Hz and 2000 Hz, etc" << endl;
// 	}
// }


// /* 
//  * Execute command with argument and second arguemnt
//  */
// void Functions::ExecuteSecArg(char *i, char *a, char *s){
//     if(s == 0){ 
//         //argument = "None";
//         cout << "No second argument given" << endl;
// 		cout << "Possible second arguments are: group1, group2, group3, group4, group5, group6, group7, group8, \n group9, group10, group11, group12, group13;" << endl;
//         return;
//     }	
	
// 	if((strcasecmp(a, "group1") == 0) && strcasecmp(s, "on") == 0){
// 		for(int i=1; i<=10; i++){
// 			cout << "Hoeksensor:" << i << " on" << endl;
// 		}
// 		set.group1 = "on";
// 		return;
// 	}


// 	if((strcasecmp(a, "group1") == 0) && strcasecmp(s, "off") == 0){
// 		for(int i=1; i<=10; i++){
// 			cout << "Hoeksensor:" << i << " off" << endl;
			
// 		}
// 		set.group1 = "off";
// 		return;
// 	}

// 	if((strcasecmp(a, "group2") == 0) && strcasecmp(s, "on") == 0){
// 		for(int i=11; i<=20; i++){
// 			cout << "Hoeksensor:" << i << " on" << endl;
// 		}
// 		set.group2 = "on";
// 		return;
// 	}


// 	if((strcasecmp(a, "group2") == 0) && strcasecmp(s, "off") == 0){
// 		for(int i=11; i<=20; i++){
// 			cout << "Hoeksensor:" << i << " off" << endl;
			
// 		}
// 		set.group2 = "off";
// 		return;
// 	}

// 	if((strcasecmp(a, "group3") == 0) && strcasecmp(s, "on") == 0){
// 		for(int i=21; i<=30; i++){
// 			cout << "Hoeksensor:" << i << " on" << endl;
// 		}
// 		set.group3 = "on";
// 		return;
// 	}


// 	if((strcasecmp(a, "group3") == 0) && strcasecmp(s, "off") == 0){
// 		for(int i=21; i<=30; i++){
// 			cout << "Hoeksensor:" << i << " off" << endl;
			
// 		}
// 		set.group3 = "off";
// 		return;
// 	}

// 	if((strcasecmp(a, "group4") == 0) && strcasecmp(s, "on") == 0){
// 		for(int i=31; i<=40; i++){
// 			cout << "Hoeksensor:" << i << " on" << endl;
// 		}
// 		set.group4 = "on";
// 		return;
// 	}


// 	if((strcasecmp(a, "group4") == 0) && strcasecmp(s, "off") == 0){
// 		for(int i=31; i<=40; i++){
// 			cout << "Hoeksensor:" << i << " off" << endl;
			
// 		}
// 		set.group4 = "off";
// 		return;
// 	}

// 	if((strcasecmp(a, "group5") == 0) && strcasecmp(s, "on") == 0){
// 		for(int i=41; i<=50; i++){
// 			cout << "Hoeksensor:" << i << " on" << endl;
// 		}
// 		set.group5 = "on";
// 		return;
// 	}


// 	if((strcasecmp(a, "group5") == 0) && strcasecmp(s, "off") == 0){
// 		for(int i=41; i<=50; i++){
// 			cout << "Hoeksensor:" << i << " off" << endl;
			
// 		}
// 		set.group5 = "off";
// 		return;
// 	}

// 	if((strcasecmp(a, "group6") == 0) && strcasecmp(s, "on") == 0){
// 		for(int i=51; i<=60; i++){
// 			cout << "Hoeksensor:" << i << " on" << endl;
// 		}
// 		set.group6 = "on";
// 		return;
// 	}


// 	if((strcasecmp(a, "group6") == 0) && strcasecmp(s, "off") == 0){
// 		for(int i=51; i<=60; i++){
// 			cout << "Hoeksensor:" << i << " off" << endl;
			
// 		}
// 		set.group6 = "off";
// 		return;
// 	}

// 	if((strcasecmp(a, "group7") == 0) && strcasecmp(s, "on") == 0){
// 		for(int i=61; i<=64; i++){
// 			cout << "Hoeksensor:" << i << " on" << endl;
// 		}
// 		set.group7 = "on";
// 		return;
// 	}


// 	if((strcasecmp(a, "group7") == 0) && strcasecmp(s, "off") == 0){
// 		for(int i=61; i<=64; i++){
// 			cout << "Hoeksensor:" << i << " off" << endl;
			
// 		}
// 		set.group7 = "off";
// 		return;
// 	}

// 	else{
// 		cout << "Argument not reconized" << endl;
// 		cout << "Possible commands are: group<N>, on or off, N=1-13" << endl;
// 	}
// }

// /*
//  * Function to sample frequency rate
//  */
// void Functions::sample(char* arg){
// 	if(arg == NULL) {
// 		cout << "Dit not get argument" << endl;
// 		return;
// 	}
// 	while(arg != NULL){
// 		int argument = atoi(arg);
// 		if(argument >= 4 && argument <= 2000){
// 			cout << "Sample frequentcy set to: " << arg << " Hz" << endl;
// 			set.samplefreq = argument;
// 			return;
// 		}
// 		else{
// 			cout << "Unknown argument: "<< arg << endl;
// 			cout << "Possible argument is between 4 Hz and 2000 Hz" << endl;
// 			return;
// 		}
// 	}
// }

// /*
//  * Function calibrate AS5162
//  */
// void Functions::kalibreer(char* arg){
// 	// if(arg == NULL) {
// 	// 	cout << "Did not get argument" << endl;
// 	// }
// 	while(arg != NULL){
// 		if(strcmp(arg, "on") == 0){
// 		    cout << "Calibrate compleet" << endl;
// 		    return;
// 		}
// 		if(strcmp(arg, "off") == 0){
// 		    cout << "Calibrate set on default" << endl;
// 		    return;
// 		}
// 		else{
// 			cout << "Unknown argument: "<< arg << endl;
// 			cout << "Possible arguments are: on, off" << endl;
// 			return;
// 		}
// 	}
// }

// /*
//  * Function download saved data
//  */
// void Functions::download(){
// 	cout << "Download started" << endl;
// }

// /*
//  * Function to set system to communication mode. 
//  * All sensors are off
//  * Download at high speed 50 Mbps, because that is the SPI top clock speed.
//  */
// void Functions::commu(){
// 	cout << "System set in commucation mode" << endl;
// 	set.activate_status = "off";
// 	set.commu_status = "on";
// 	set.group1 = "off"; 
// 	set.group2 = "off"; 
// 	set.group3 = "off"; 
// 	set.group4 = "off"; 
// 	set.group5 = "off"; 
// 	set.group6 = "off"; 
// 	set.group7 = "off";
// 	set.samplefreq = 2000;
// 	set.odo_status = "off";
// 	set.druk_status = "off";
// 	set.gyroacc_status = "off";
// 	set.Boost = "off";
// 	set.Buck = "off";
// }

// /*
//  * Function to set system in activation mode.
//  * All sensors are on.
//  * Reading data are being saved.
//  */
// void Functions::acti(){
// 	cout << "System set in activation modue" << endl;
// 	set.activate_status = "on";
// 	set.commu_status = "off";
// 	set.group1 = "on"; 
// 	set.group2 = "on"; 
// 	set.group3 = "on"; 
// 	set.group4 = "on"; 
// 	set.group5 = "on"; 
// 	set.group6 = "on"; 
// 	set.group7 = "on";
// 	set.samplefreq = 2000;
// 	set.odo_status = "on";
// 	set.druk_status = "on";
// 	set.gyroacc_status = "on";
// 	set.Boost = "on";
// 	set.Buck = "on";
// }

// /*
//  * Function to turn odometers on or off
//  */
// void Functions::odo(char* arg){
// 	// if(arg == NULL) {
// 	// 	cout << "Did not get argument" << endl;
// 	// }
// 	while(arg != NULL){
// 		if(strcmp(arg, "on") == 0){
// 		    cout << "Odometers on" << endl;
// 			set.odo_status = "on";
// 		    return;
// 		}
// 		if(strcmp(arg, "off") == 0){
// 		    cout << "Odometers off" << endl;
// 			set.odo_status = "off";
// 		    return;
// 		}
// 		else{
// 			cout << "Unknown argument: "<< arg << endl;
// 			cout << "Possible arguments are: on, off" << endl;
// 			return;
// 		}
// 	}
// }

// /*
//  * Function to turn pressure sensors on or off
//  */
// void Functions::druk(char* arg){
// 	// if(arg == NULL) {
// 	// 	cout << "Did not get argument" << endl;
// 	// }
// 	while(arg != NULL){
// 		if(strcmp(arg, "on") == 0){
// 		    cout << "Druksensoren on" << endl;
// 			set.druk_status = "on";
// 		    return;
// 		}
// 		if(strcmp(arg, "off") == 0){
// 		    cout << "Druksensoren off" << endl;
// 			set.druk_status = "off";
// 		    return;
// 		}
// 		else{
// 			cout << "Unknown argument: "<< arg << endl;
// 			cout << "Possible arguments are: on, off" << endl;
// 			return;
// 		}
// 	}
// }

// /*
//  * Function to turn Gyroscopes and Accelerometers on or off
//  */
// void Functions::GyroAcc(char* arg){
// 	// if(arg == NULL) {
// 	// 	cout << "Dit not get argument" << endl;
// 	// }
// 	while(arg != NULL){
// 		if(strcmp(arg, "on") == 0){
// 		    cout << "GyroAcc on" << endl;
// 			set.gyroacc_status = "on";
// 		    return;
// 		}
// 		if(strcmp(arg, "off") == 0){
// 		    cout << "GyroAcc off" << endl;
// 			set.gyroacc_status = "off";
// 		    return;
// 		}
// 		else{
// 			cout << "Unknown argument: "<< arg << endl;
// 			cout << "Possible arguments are: on, off" << endl;
// 			return;
// 		}
// 	}
// }

// /*
//  * Function to turn power supply off.
//  * All sensors are turn off.
//  * MCU remain in low power mode.
//  */
// void Functions::voeding(char* arg){
// 	// if(arg == NULL) {
// 	// 	cout << "Dit not get argument" << endl;
// 	// }
// 	while(arg != NULL){
// 		if(strcmp(arg, "on") == 0){
// 		    cout << "All sensors are on" << endl;
// 			set.Boost = "on";
// 			set.Buck = "on";
// 		    return;
// 		}
// 		if(strcmp(arg, "off") == 0){
// 		    cout << "All sensors are off" << endl;
// 			set.Boost = "off";
// 			set.Buck = "off";
// 		    return;
// 		}
// 		else{
// 			cout << "Unknown argument: "<< arg << endl;
// 			cout << "Possible arguments are: on, off" << endl;
// 			return;
// 		}
// 	}
// }

// /*
//  * Function to turn read to voltage of the battery
//  * Function to read temperature at three differentes positions.
//  * Position 1: Cable at the front of the module .
//  * Position 2: Cable at the back of the module.
//  * Position 3: On-board. 
//  */
// void Functions::read(char* arg){
// 	// if(arg == NULL) {
// 	// 	cout << "Dit not get argument" << endl;
// 	// }
// 	while(arg != NULL){
// 		if(strcmp(arg, "voltage") == 0){
// 		    cout << "Voltage of the battery is 5 V" << endl;
// 		    return;
// 		}
// 		if(strcmp(arg, "temp") == 0){
// 		cout << "Temp position 1: " << "25 C" << endl;
// 		cout << "Temp position 2: " << "26 C" << endl;
// 		cout << "Temp position 3: " << "30 C" << endl;
// 		    return;
// 		}
// 		else{
// 			cout << "Unknown argument: "<< arg << endl;
// 			cout << "Possible arguments are: voltage, temp" << endl;
// 			return;
// 		}
// 	}
// }