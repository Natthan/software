#define MaxCaliperSensors 63
#define MaxOdoSensors 4
#define MaxPresSensors 3
#define MaxTempSensors 4
#define MaxAxisMem 6

typedef struct
{
    int communication_mode_flag, activation_mode_flag;
    int J[MaxCaliperSensors], J_buf[MaxCaliperSensors];
    int odo[MaxOdoSensors], odo_buf[MaxOdoSensors];
    int Press[MaxPresSensors], Press_buf[MaxPresSensors];
    int Temp[MaxTempSensors], Temp_buf[MaxTempSensors];
    int battery_voltage;
    int samplefreq;
    int stats; 
    int FiveVolt, ThreeVolt;
    int IMU; char IMU_buf[MaxAxisMem];
    int date, month, year, sec, minutes, hour;
} SensorsStats;

SensorsStats set;
