#include "LinkList.h"

TrieNode *Build_Trienode(char data){
    TrieNode* node = (TrieNode*) malloc(sizeof(TrieNode));
    for (int i=0; i<N; i++)
        node->children[i] = NULL;
    node->is_leaf = 0;
    node->data = data;
    return node;
}

void Free_Trienode(TrieNode* node){
    for(int i=0; i<N; i++) {
        if (node->children[i] != NULL) {
            Free_Trienode(node->children[i]);
        }
        else {
            continue;
        }
    }
    free(node);
}

TrieNode* Insert_Command_Trie(TrieNode* root, char* word, command_function functie){
    TrieNode* temp = root;

    for (int i=0; word[i] != '\0'; i++) {
        // Get the relative position in the alphabet list
    	int idx = (int) word[i] - 'a';
        if (temp->children[idx] == NULL) {
            // If the corresponding child doesn't exist,
            // simply create that child!
            temp->children[idx] = Build_Trienode(word[i]);
        }
        else {
            // Do nothing. The node already exists
        }
        // Go down a level, to the child referenced by idx
        // since we have a prefix match
        temp = temp->children[idx];
    }
    // At the end of the word, mark this node as the leaf node
    temp->is_leaf = 1;
    // At the end of the word, mark this node as the function
    temp->function = functie;
    return root;
}

// int search_trie_test(TrieNode* root, char* word, char* spatie){
//     TrieNode* temp = root;

//     for(int i=0; word[i]!='\0'; i++){
//     	int position = word[i] - 'a';
//         if (temp->children[position] == NULL){
//         	return 0;
//         }
//         temp = temp->children[position];
//     }
//     if (temp != NULL && temp->is_leaf == 1){
//     	fp = temp->function;
//     	fp(spatie);
//     	return 1;
//     }
//     return 0;
// }

int Search_Command_Trie_Without_Arg(TrieNode* root, char* word){
    TrieNode* temp = root;

    for(int i=0; word[i]!='\0'; i++){
    	int position = word[i] - 'a';
        if (temp->children[position] == NULL){
            printf("Did not found the entered function!\r\n");
            printf("Enter help for instructions.\r\n");
        	return 0;
        }
        temp = temp->children[position];
    }
    if (temp != NULL && temp->is_leaf == 1){
        // fp = temp->function;
    	// fp(first_arg, sec_arg);
    	return 1;
    }
    return 0;
}

void Print_Builded_Trie(TrieNode* root){
    if (!root)
    	return;
    TrieNode* temp = root;
    printf("%c -> ", temp->data);
    for (int i=0; i<N; i++) {
        Print_Builded_Trie(temp->children[i]);
    }
}

TrieNode *Build_Command_Trie(TrieNode* root){
	root = Insert_Command_Trie(root, (char*)"s", &SetSample);
    root = Insert_Command_Trie(root, (char*)"cp", &PowerCalib);
    root = Insert_Command_Trie(root, (char*)"d", &download);
    root = Insert_Command_Trie(root, (char*)"i", &PowerMem);
    root = Insert_Command_Trie(root, (char*)"o", &PowerOdo);
    root = Insert_Command_Trie(root, (char*)"pr", &PowerPress);
    root = Insert_Command_Trie(root, (char*)"pow", &PowerSensor);
    root = Insert_Command_Trie(root, (char*)"t", &PowerTemp);
    root = Insert_Command_Trie(root, (char*)"r", &ReadData);
    root = Insert_Command_Trie(root, (char*)"cb", &Calibrate);
    root = Insert_Command_Trie(root, (char*)"cm", &CommunicationMode);
    root = Insert_Command_Trie(root, (char*)"rtc", &RTC_init);
    root = Insert_Command_Trie(root, (char*)"on", &AllSensorsOn);
    root = Insert_Command_Trie(root, (char*)"help", &DisplayInfo);
    //print_trie(root);
    //printf("\n");
    return root;
}


