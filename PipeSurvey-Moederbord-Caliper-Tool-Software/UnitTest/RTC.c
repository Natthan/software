#include "RTC.h"
 
int function_fm(int date, int month, int year) {
   int fmonth, leap;
 
   //leap function 1 for leap & 0 for non-leap
   if ((year % 100 == 0) && (year % 400 != 0))
      leap = 0;
   else if (year % 4 == 0)
      leap = 1;
   else
      leap = 0;
 
   fmonth = 3 + (2 - leap) * ((month + 2) / (2 * month))
         + (5 * month + month / 9) / 2;
 
   //bring it in range of 0 to 6
   fmonth = fmonth % 7;
 
   return fmonth;
}
 
//----------------------------------------------
int day_of_week(int date, int month, int year) {
 
   int dayOfWeek;
   int YY = year % 100;
   int century = year / 100;
 
   //printf("\nDate: %d/%d/%d \n", date, month, year);
 
   dayOfWeek = 1.25 * YY + function_fm(date, month, year) + date - 2 * (century % 4);
 
   //remainder on division by 7
   dayOfWeek = dayOfWeek % 7;
 
   // switch (dayOfWeek) {
   //    case 0:
   //       printf("weekday = Saturday");
   //       break;
   //    case 1:
   //       printf("weekday = Sunday");
   //       break;
   //    case 2:
   //       printf("weekday = Monday");
   //       break;
   //    case 3:
   //       printf("weekday = Tuesday");
   //       break;
   //    case 4:
   //       printf("weekday = Wednesday");
   //       break;
   //    case 5:
   //       printf("weekday = Thursday");
   //       break;
   //    case 6:
   //       printf("weekday = Friday");
   //       break;
   //    default:
   //       printf("Incorrect time data!\r\nTry calling the function again with the correct iput!\r\n");
   // }
   return 0;
}

int RTC_Test(int sec, int min, int hour, int date, int month, int year){
    if(sec < 0 || sec > 60){
        //printf("The entered seconds must be in beteween 0 and 59 seconds!\r\nTry calling the function again with the correct iput!\r\n");
        return 0;
    }

    if(min < 0 || min > 60){
        //printf("The entered minutes must be in beteween 0 and 59 seconds!\r\nTry calling the function again with the correct iput!\r\n");
        return 0;
    }

    if(hour < 0 || hour > 23){
        //printf("The entered hour must be in beteween 0 and 23 military hour!\r\nTry calling the function again with the correct iput!\r\n");
        return 0;
    }

    if(date <= 0 || date > 31) return 0;

    if(month <= 0 || month > 12) return 0;

    if(year < 0 || year > 2022) return 0;

    day_of_week(date, month, year);
   //  printf("\r\nSettled RTC time and date-> Time: %d:%d:%d | Date: %d/%d/%d\r\n", hour, min, sec,
   //                                                                           date, month, year);
   return 1;
}


// //------------------------------------------
// int main() {
//    int date, month, year;
 
//    printf("\nEnter the year ");
//    scanf("%d", &year);
 
//    printf("\nEnter the month ");
//    scanf("%d", &month);
 
//    printf("\nEnter the date ");
//    scanf("%d", &date);
 
//    day_of_week(date, month, year);
 
//    return 0;
// }