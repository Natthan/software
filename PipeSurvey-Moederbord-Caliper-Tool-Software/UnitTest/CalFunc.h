#ifndef CALFUNC_H
#define CALFUNC_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "RTC.h"
#include "RTC.c"

#include "SensorsStats.h"

void DisplayInfo(char *first_a, char *sec);

int SetSample_Test(int sample);

int PowerCalib_Test(int num, int stats);

int PowerOdo_Test(int number, int status);

int PowerTemp_Test(int num, int stats);

int PowerMem_Test(int num, int stats);

int PowerPress_Test(int num, int stats);

int Calibrate_Test(int d);

int ReadBattery_Test(int volt);

void AllSensorsOn(char* first_a, char *sec_a);

int ch_calfunc = 0;
char GetLengthCaliperArmBuf[5] = {0};
int i_calfunc = 0;

#endif