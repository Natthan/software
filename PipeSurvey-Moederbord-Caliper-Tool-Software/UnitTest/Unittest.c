#include <stdio.h>
#include <windows.h>

#include "CalFunc.h"
#include "CalFunc.c"

#include "LinkList.h"
#include "LinkList.c"

static int tests = 0;
static int tests_cnt = 0;
static int fails = 0;
static int fails_cnt = 0;

static int TestNumber = 1;

TrieNode *CommandTrie;

#define TEST(condition, ...) \
    tests++; tests_cnt++;\
    if (!(condition))\
    {\
        fails++; fails_cnt++;\
        printf("Error: ");\
        printf(__VA_ARGS__);\
        printf("\r\n");\
    }\
    //else printf("Succeeded!\r\n");

#define PRINT_TEST_REPORT\
    printf("%d tests performed: %d succeded, %d failed.\n", tests, tests - fails, fails);

#define PRINT_TEST_ALL_REPORT\
    printf("%d tests performed: %d succeded, %d failed.\n", tests_cnt, tests_cnt - fails_cnt, fails_cnt);

const int max_size_test1 = 14;
char TestCommandList[max_size_test1][max_size_test1] = {"s","cp","d","i","o","pr","pow","t","r","cb","cm","rtc","on","help"};
void TestCommand(){
    printf("\t\t\tTest %d begin: Check if command is being reconized\r\n", TestNumber);
    for(int i=0; i<max_size_test1; i++){
        TEST(Search_Command_Trie_Without_Arg(CommandTrie, TestCommandList[i]), "Command is not being reconized");
    }
    PRINT_TEST_REPORT
    printf("\t\t\t\t\tTest %d end\r\n\r\n", TestNumber++);
}

int sec, mint, hour;
void TestRtCInput(){
    tests = 0, fails = 0;
    printf("\t\t\tTest %d begin: Check RTC function with some random values\r\n", TestNumber);
    for(int i=0; i<5; i++){
        sec = rand() % 59; mint = rand() % 59; hour = rand() % 24;
        date = rand() % 31; month = rand() % 12; year = rand() % 2022;
        TEST(RTC_Test(sec, mint, hour, date, month, year), "The input time was not right!");
        if(RTC_Test(sec, mint, hour, date, month, year) == 0) printf("Settled RTC time and date-> Time: %d:%d:%d | Date: %d/%d/%d\r\n", hour, mint, sec, date, month, year);

    }
    PRINT_TEST_REPORT
    printf("\t\t\t\t\tTest %d end\r\n\r\n", TestNumber++);
}

void TestSampleFunction(){
    tests = 0, fails = 0;
    printf("\t\t\tTest %d begin: Check set sample function with some random values\r\n", TestNumber);
    for(int i=0; i<5; i++){
        int samp = rand() % 2200;
        TEST(SetSample_Test(samp), "Input sample is not between 4 Hz and 2000 Hz");
        if(SetSample_Test(samp) != 1) printf("%d Hz is not withing the range\r\n", samp);
    }
    PRINT_TEST_REPORT
    printf("\t\t\t\t\tTest %d end\r\n\r\n", TestNumber++);
}

void TestOdo(){
    tests = 0, fails = 0;
    printf("\t\t\tTest %d begin: Check odo functionality\r\n", TestNumber);
    printf("\t\tSetting the odo sensors to 1 (on) and 0 (off)!\r\n");
    for(int i=1; i<=4; i++){
        TEST(PowerOdo_Test(i, 1), "Could not set the odo sensor %d to 1!", i);
        TEST(PowerOdo_Test(i, 0), "Could not set the odo sensor %d to 0!", i);
    }
    PRINT_TEST_REPORT
    printf("\t\t\t\t\tTest %d end\r\n\r\n", TestNumber++);
}

void TestCaliperSensors(){
    tests = 0, fails = 0;
    printf("\t\t\tTest %d begin: Check caliper functionality\r\n", TestNumber);
    printf("\t\t\tSetting the caliper sensors to 1 (on) and 0 (off)!\r\n");
    for(int i=0; i<=16; i++){
        TEST(PowerCalib_Test(i , 1), "Could not set the caliper sensor %d to 1!", i);
        TEST(PowerCalib_Test(i , 0), "Could not set the caliper sensor %d to 0!", i);
    }
    PRINT_TEST_REPORT
    printf("\t\t\t\t\tTest %d end\r\n\r\n", TestNumber++);
}

void TestIMU(){
    tests = 0, fails = 0;
    printf("\t\t\tTest %d begin: Check IMU functionality\r\n", TestNumber);
    printf("\t\tSetting the IMU sensor to 1 (on) and 0 (off)!\r\n");
    for(int i=1; i<=2; i++){
        TEST(PowerMem_Test(i, 1), "Could not set the IMU sensor %d to 1!", i);
        TEST(PowerMem_Test(i, 0), "Could not set the IMU sensor %d to 0!", i);
    }
    PRINT_TEST_REPORT
    printf("\t\t\t\t\tTest %d end\r\n\r\n", TestNumber++);
}

void TestPress(){
    tests = 0, fails = 0;
    printf("\t\t\tTest %d begin: Check pressure sensor functionality\r\n", TestNumber);
    printf("\t\tSetting the pressure sensors to 1 (on) and 0 (off)!\r\n");
    for(int i=1; i<=4; i++){
        TEST(PowerPress_Test(i, 1), "Could not set the pressure sensor %d to 1!", i);
        TEST(PowerPress_Test(i, 0), "Could not set the pressure sensor %d to 0!", i);
    }
    PRINT_TEST_REPORT
    printf("\t\t\t\t\tTest %d end\r\n\r\n", TestNumber++);
}

void TestBattery(){
    tests = 0, fails = 0;
    printf("\t\t\tTest %d begin: Check battery functionality\r\n", TestNumber);
    printf("\t\tSetting the battery voltage level to under 4 Volt expected 1!\r\n");
    TEST(ReadBattery_Test(3) == 1, "The error voltage was not detected!");
    printf("\t\tSetting the battery voltage level to above 4 Volt expected 0!\r\n");
    TEST(ReadBattery_Test(5) == 0, "The error voltage was not detected!");
    PRINT_TEST_REPORT
    printf("\t\t\t\t\tTest %d end\r\n\r\n", TestNumber++);
}

//     printf("Test argugment for function activation\r\n");
//     for(int i=0; i<3; i++){
//         printf("Argument: %s\r\n", test2[i]);
//         TEST(call.CheckArg("activation", test2[i]), "Argument is not being reconized\r\n");
//     }

//     printf("Test argugment for function communication\r\n");
//     for(int i=0; i<3; i++){
//         printf("Argument: %s\r\n", test2[i]);
//         TEST(call.CheckArg("communication", test2[i]), "Argument is not being reconized\r\n");
//     }

int main(void)
{
    CommandTrie = Build_Trienode('\0');

    Build_Command_Trie(CommandTrie);

    while(1){
        TestCommand();
        TestRtCInput();
        TestSampleFunction();
        TestCaliperSensors();
        TestOdo();
        TestIMU();
        TestBattery();
        PRINT_TEST_ALL_REPORT
        Sleep(100000);
    }
    //return 0;
}