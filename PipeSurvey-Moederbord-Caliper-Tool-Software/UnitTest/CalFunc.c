#include "CalFunc.h"

/*----------------------------------------------------------------------------------------------------*/
void DisplayInfo(char *first_a, char *sec_a){
    printf("To call a function: \r\n\r\n<command><first argument><second argument><third argument>\r\n\r\nCommand list below: \r\n\r\n");

    printf("Activation mode (info: there will be no display of data from the sensors" 
           "\r\nand no function other than communication mode will work):" 
           "\r\n<act><no argument needed><no arugment needed>\r\n\r\n");
    printf("Calibration (info: to calibrate the caliper sensors):" 
           "\r\n<cb><no argument needed><no arugment needed>\r\n\r\n");
    printf("Caliper sensors on or off (info: one sensor group consists of 4 caliper sensors):" 
           "\r\n<cp><0-15><1/0>\r\n\r\n");
    printf("Communication mode (info: the setting functions are available):"
           "\r\n<com><no argument needed><no arugment needed>\r\n\r\n");
    printf("Download saved data:"
           "\r\n<d><no argument needed><no argument needed>\r\n\r\n");
    printf("IMU on or off:" 
           "\r\n<i><1/0><no argument needed>\r\n\r\n");
    printf("Odometers on or off (info: there are 3 odometers):"
           "\r\n<o><1-3><1/0>\r\n\r\n");
    printf("Pressure sensors on or off (info: there are 2 pressure sensors):" 
           "\r\n<pr><1-2><1/0>\r\n\r\n");
    printf("Power 5V or 3V3 on or off (info: the power of the MCU will not be turn off):"
           "\r\n<pow><5/3><1/0>\r\n\r\n");
    printf("Sample frequency: (info: to settle frequency rate):"
            "\r\n<s><4Hz-2000Hz><no argument needed>\r\n\r\n");

    printf("To read the output data from all sensors (info: this function will display one measurement of all sensors):"
           "\r\n<r><no argument needed><no arugment needed>\r\n\r\n");
}

/*----------------------------------------------------------------------------------------------------*/
void SetSample(char *first_a, char *sec_a){
    if(first_a == NULL){
        printf("You did not entered sample freq\r\n");
        printf("Enter freq between 4 Hz and 2000 Hz\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }

    if(sec_a != NULL){
        printf("Second argument is not needed\r\n");
        int error = atoi(sec_a);
        printf("%d will not be applied to the function\r\n", error);
        return;
    }

    if(first_a != NULL){
        int samp = atoi(first_a);
        set.samplefreq = atoi(first_a);
		if(set.samplefreq >= 4 && set.samplefreq <= 2000){
			printf("Sample frequency set to: %d Hz\r\n", set.samplefreq);
			set.samplefreq = samp;
			return;
		}
		else{
			printf("Unknown argument: %d\r\n", set.samplefreq);
            printf("Enter freq between 4 Hz and 2000 Hz\r\nTry calling the function again with the correct iput!\r\n");
			return;
		}
    }
    return;
}

int SetSample_Test(int samplefreq){
    if(samplefreq >= 4 && samplefreq <= 2000){
        return 1;
    }
    else{
        return 0;
    }
    return 0;
}

/*----------------------------------------------------------------------------------------------------*/
void RTC_init(char *first_a, char *sec_a){
    printf("\nEnter the seconds ");
    scanf("%d", &set.sec);
    if(set.sec < 0 && set.sec > 60){
        printf("The entered seconds must be in beteween 0 and 59 seconds!\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }

    printf("\nEnter the minutes ");
    scanf("%d", &set.minutes);
    if(set.minutes < 0 && set.minutes > 60){
        printf("The entered minutes must be in beteween 0 and 59 seconds!\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }

    printf("\nEnter the hour ");
    scanf("%d", &set.hour);
    if(set.hour < 0 && set.hour > 23){
        printf("The entered hour must be in beteween 0 and 23 military hour!\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }

    printf("\nEnter the year ");
    scanf("%d", &set.year);

    printf("\nEnter the month ");
    scanf("%d", &set.month);

    printf("\nEnter the date ");
    scanf("%d", &set.date);

    day_of_week(set.date, set.month, set.year);
    printf("\r\nSettled RTC time and date-> Time: %d:%d:%d | Date: %d/%d/%d\r\n", set.hour, set.minutes, set.sec,
                                                                              set.date, set.month, set.year);
}

/*----------------------------------------------------------------------------------------------------*/
int ReadCalibSensors(int number){
    // int val = 0;
    if(set.J[number] == 1 && set.FiveVolt == 1) return set.J_buf[number] = (rand() % 4096);
    //else printf("5V stats: %d \t | \t J%d stats: %d\r\n", set.FiveVolt, number, set.J[number]);
    return set.J_buf[number];
    // for(int i=0; i<MaxCaliperSensors; i++){
    //     printf("Caliper sensor %d: %d\r\n", i, (set.J_buf[i] = (rand() % 4096)));
    // }
}

void PowerCalib(char *first_a, char *sec_a){
    if(set.FiveVolt != 1){
        return;
    }
    if(first_a == NULL){ 
        printf("No group<0-15> given\r\n");
        printf("hint: To turn caliper sensors <0-3> on -> c 0 1\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }

    if(strcasecmp(first_a, "reset") == 0 || strcasecmp(first_a, "r") == 0){
        memset(set.J_buf, 0, sizeof(set.J_buf));
        printf("Reset the values of the caliper sensors: done!\r\n");
        return;
    } 

    if(sec_a == NULL){
        printf("No second argument given <1/0>\r\n");
        printf("hint: To turn caliper sensors <4-7> on -> c 2 1\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }

    if(strcasecmp(first_a, "a") == 0 || strcasecmp(first_a, "all") == 0){
        set.stats = atoi(sec_a);
        switch (set.stats)
        {
        case 0: for(int i=0; i<(MaxCaliperSensors); i++) (set.J[i] = set.stats); break; //printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats)); break;
        case 1: for(int i=0; i<(MaxCaliperSensors); i++) (set.J[i] = set.stats); break; //printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats)); break;
        default: printf("The second argument can not be less than 0 or greater than 1\r\nTry calling the function again with the correct iput!\r\n"); break;
        }
        return;
    }

    int group = atoi(first_a);
//    set.stats = atoi(sec_a);

    if(set.stats < 0 && set.stats >= 15){
        printf("There are no group less than 0 or greater than 15!\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }

    switch (group)
    {
    case 0: for(int i=0; i<(0+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 1: for(int i=4; i<(4+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 2: for(int i=8; i<(8+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 3: for(int i=12; i<(12+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 4: for(int i=16; i<(16+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 5: for(int i=20; i<(20+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 6: for(int i=24; i<(24+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 7: for(int i=28; i<(28+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 8: for(int i=32; i<(32+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 9: for(int i=36; i<(36+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 10: for(int i=40; i<(40+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 11: for(int i=44; i<(44+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 12: for(int i=48; i<(48+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 13: for(int i=52; i<(52+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 14: for(int i=56; i<(56+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    case 15: for(int i=60; i<(60+4); i++) printf("Caliper sensor %d: %d\r\n", i, (set.J[i] = set.stats));break;
    default: printf("Entered incorrect!\r\nType help for instructions\r\n"); break;
    }
}

int PowerCalib_Test(int number, int stats){
    if(stats < 0 || stats >= 15){
        return 0;
    }

    switch (number)
    {
    case 0: return 1; break;
    case 1: return 1; break;
    case 2: return 1; break;
    case 3: return 1; break;
    case 4: return 1; break;
    case 5: return 1; break;
    case 6: return 1; break;
    case 7: return 1; break;
    case 8: return 1; break;
    case 9: return 1; break;
    case 10: return 1; break;
    case 11: return 1; break;
    case 12: return 1; break;
    case 13: return 1; break;
    case 14: return 1; break;
    case 15: return 1; break;
    default: printf("Caliper %d does not exist!\r\n", number); return 0; break;
    }
    return 0;
}


/*----------------------------------------------------------------------------------------------------*/
void Calibrate(char *first_a, char *sec_a){
    if(first_a != NULL){ 
        printf("No first argument needed!\r\n");
        printf("The first argument will not be applied to the function\r\n");
        return;
    }

    if(sec_a != NULL){
        printf("No second argument needed!\r\n");
        printf("The second argument will not be applied to the function\r\n");
        return;
    }

    printf("What is the length of the caliper arm?\r\n");
    while((ch_calfunc = getchar()) != '\n'){
        GetLengthCaliperArmBuf[i_calfunc++] = ch_calfunc;
    }
    int val = atoi(GetLengthCaliperArmBuf);

    if(val < 0 || val > 30){
        printf("The arm length can not be under 0 cm or above 30 cm!\r\nTry calling the function again with the correct iput!\r\n");
        val = 0;
        i_calfunc = 0;
        memset(GetLengthCaliperArmBuf, 0, sizeof(GetLengthCaliperArmBuf));
        return;
    }

    printf("The length of the caliper arm is: %d cm", val);
    memset(GetLengthCaliperArmBuf, 0, sizeof(GetLengthCaliperArmBuf));
    i_calfunc = 0;
    val = 0;
}

int Calibrate_Test(int d){
    if(d < 0 || d > 30) return 0;
    else return 1;
}


/*----------------------------------------------------------------------------------------------------*/
void download(char *first_a, char *sec_a){
    printf("Donwload compleet!\r\n");
}

/*----------------------------------------------------------------------------------------------------*/
void CommunicationMode(char *first_a, char * sec_a){
    set.communication_mode_flag = 1; set.activation_mode_flag = 0;
    // if(set.activation_mode_flag == 1 || set.communication_mode_flag == 0){
    //     printf("Activation mode still in process!\r\n");
    //     return;
    // }

    // while(set.activation_mode_flag == 0 && set.communication_mode_flag == 1){
    // }
}

// /*
//  * Function to set system to communication mode. 
//  * All sensors are off
//  * Download at high speed 40 Mbps, because that is the SPI top clock speed.
//  */
// void Functions::commu(){
// 	cout << "System set in commucation mode" << endl;
// 	set.activate_status = "off";
// 	set.commu_status = "on";
// 	set.group1 = "off"; 
// 	set.group2 = "off"; 
// 	set.group3 = "off"; 
// 	set.group4 = "off"; 
// 	set.group5 = "off"; 
// 	set.group6 = "off"; 
// 	set.group7 = "off";
// 	set.samplefreq = 2000;
// 	set.odo_status = "off";
// 	set.druk_status = "off";
// 	set.gyroacc_status = "off";
// 	set.Boost = "off";
// 	set.Buck = "off";
// }

// /*
//  * Function to set system in activation mode.
//  * All sensors are on.
//  * Reading data are being saved.
//  */
// void Functions::acti(){
// 	cout << "System set in activation modue" << endl;
// 	set.activate_status = "on";
// 	set.commu_status = "off";
// 	set.group1 = "on"; 
// 	set.group2 = "on"; 
// 	set.group3 = "on"; 
// 	set.group4 = "on"; 
// 	set.group5 = "on"; 
// 	set.group6 = "on"; 
// 	set.group7 = "on";
// 	set.samplefreq = 2000;
// 	set.odo_status = "on";
// 	set.druk_status = "on";
// 	set.gyroacc_status = "on";
// 	set.Boost = "on";
// 	set.Buck = "on";
// }

/*----------------------------------------------------------------------------------------------------*/
int ReadOdoSensors(int number){
    //for(int i=1; i<MaxOdoSensors; i++) printf("Odo %d: %d \t | \t ", i, (set.odo_buf[i] = (rand() % 4096)));
    if(set.odo[number] == 1 && set.FiveVolt ==1) return set.odo_buf[number] = (rand() % 4096);
    //else printf("5V stats: %d \t | \t Odo%d stats: %d", set.FiveVolt, number, set.odo[number]);
    return set.odo_buf[number];
}

void PowerOdo(char *first_a, char *sec_a){
    if(set.FiveVolt != 1){
        printf("5V stats: %d\r\n(hint: pow 5 1)\r\n", set.FiveVolt);
        return;
    }

    if(first_a == NULL){ 
        printf("No odo<1-3> given\r\n");
        printf("hint: To turn odo1> on -> o 1 1\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }	

    if(sec_a == NULL){
        printf("No second argument given <1/0>\r\n");
        printf("hint: To turn odo3 off -> o 3 0\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }	

    if(strcasecmp(first_a, "a") == 0 || strcasecmp(first_a, "all") == 0){
        set.stats = atoi(sec_a);
        switch (set.stats)
        {
        case 0: for(int i=1; i<(MaxOdoSensors); i++) set.odo[i] = set.stats; break; //printf("Odo sensor %d: %d\r\n", i, (set.odo[i] = set.stats)); break;
        case 1: for(int i=1; i<(MaxOdoSensors); i++) set.odo[i] = set.stats; break; //printf("Odo sensor %d: %d\r\n", i, (set.odo[i] = set.stats)); break;
        default: printf("The second argument can not be less than 0 or greater than 1\r\nTry calling the function again with the correct iput!\r\n"); break;
        }
        return;
    }

    int number = atoi(first_a);
//    set.stats = atoi(sec_a);

    if(set.stats < 0 && set.stats >= 3){
        printf("The second argument is no less than 0 or greather than 1!\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }

    switch (number)
    {
    case 1: set.odo[1] = set.stats; printf("Odo sensor %d: %d\r\n", number, set.stats);break;
    case 2: set.odo[2] = set.stats; printf("Odo sensor %d: %d\r\n", number, set.stats);break;
    case 3: set.odo[3] = set.stats; printf("Odo sensor %d: %d\r\n", number, set.stats);break;
    default: printf("The number of the component is no less than 0 or greater than 3!\r\nTry calling the function again with the correct iput!\r\n"); break;
    }
}

int PowerOdo_Test(int number, int stats){
    if(stats < 0 || stats > 1) return 0;

    switch (number)
    {
    case 1: return 1; break;
    case 2: return 1; break;
    case 3: return 1; break;
    default: printf("Odo %d does not exist!\r\n", number); return 0; break;
    }
}

/*----------------------------------------------------------------------------------------------------*/
int ReadTempSensors(int number){
    if(set.Temp[number] == 1) return set.Temp_buf[number] = (rand() % 100);
    //else printf("3V3 stats: %d \t | \t Temp%d stats: %d", set.ThreeVolt, number, set.Temp[number]);
    return set.Temp_buf[number];
    //printf("Temp sensor %d: %d \t | \t", i, (set.Temp[i] = (rand() % 100)));
}

void PowerTemp(char *first_a, char *sec_a){
    if(set.ThreeVolt != 1){
        printf("3V3 stats: %d\r\n(hint: pow 3 1)\r\n", set.ThreeVolt);
        return;
    }

    if(first_a == NULL){ 
        printf("No temp<1-3> given\r\n");
        printf("hint: To turn temp2 on -> t 2 1\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }	

    if(sec_a == NULL){
        printf("No second argument given <1/0>\r\n");
        printf("hint: To turn temp3 off -> t 3 0\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }
    
    if(strcasecmp(first_a, "a") == 0 || strcasecmp(first_a, "all") == 0){
        set.stats = atoi(sec_a);
        switch (set.stats)
        {
        case 0: for(int i=1; i<(MaxTempSensors); i++) set.Temp[i] = set.stats; break; //printf("Temp sensor %d: %d\r\n", i, (set.Temp[i] = set.stats)); break;
        case 1: for(int i=1; i<(MaxTempSensors); i++) set.Temp[i] = set.stats; break; //printf("Temp sensor %d: %d\r\n", i, (set.Temp[i] = set.stats)); break;
        default: printf("The second argument can not be less than 0 or greater than 1\r\nTry calling the function again with the correct iput!\r\n"); break;
        }
        return;
    }	

    int number = atoi(first_a);
//    set.stats = atoi(sec_a);

    if(set.stats < 0 && set.stats >= 3){
        printf("The second argument is no less than 0 or greater than 1!\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }

    switch (number)
    {
    case 1: set.Temp[1] = set.stats; printf("Temp sensor %d: %d\r\n", number, set.stats);break;
    case 2: set.Temp[2] = set.stats; printf("Temp sensor %d: %d\r\n", number, set.stats);break;
    case 3: set.Temp[3] = set.stats; printf("Temp sensor %d: %d\r\n", number, set.stats);break;
    default: printf("The number of the component is no less than 0 or greater than 3!\r\nTry calling the function again with the correct iput!\r\n"); break;
    }
}

int PowerTemp_Test(int number, int stats){
    if(stats < 0 || stats >= 3) return 0;

    switch (number)
    {
    case 1: return 1; break;
    case 2: return 1; break;
    case 3: return 1; break;
    default: printf("Temp sensor %d does not exist!\r\n", number); return 0; break;
    }
}


/*----------------------------------------------------------------------------------------------------*/
int ReadMem(){
    int ax, ay, az, gx, gy, gz;
    // printf("Ax: %0.4lf \t | \t Ay: %0.4lf \t | \t Az: %0.4lf\r\n", ((rand() % 2001 - 1000) / 2.e3));
    // printf("Gx: %0.4lf \t | \t Gy: %0.4lf \t | \t Gz: %0.4lf\r\n", ((rand() % 2001 - 1000) / 2.e3));
    if(set.IMU == 1) return
    sprintf(set.IMU_buf, "Ax: %0.4lf \t | \t Ay: %0.4lf \t | \t Az: %0.4lf\r\nGx: %0.4lf \t | \t Gy: %0.4lf \t | \t Gz: %0.4lf\r\n", 
    ax = ((rand() % 1001) / 2.e3), ay = ((rand() % 1001) / 2.e3), az = ((rand() % 1001) / 2.e3),
    gx = ((rand() % 1001) / 2.e3), gy = ((rand() % 1001) / 2.e3), gz = ((rand() % 1001) / 2.e3));

    // ax = ((rand() % 2001 - 1000) / 2.e3), ay = ((rand() % 2001 - 1001) / 2.e3), az = ((rand() % 2001 - 1000) / 2.e3),
    // gx = ((rand() % 2001 - 1000) / 2.e3), gy = ((rand() % 2001 - 1000) / 2.e3), gz = ((rand() % 2001 - 1000) / 2.e3));

    //else printf("3V3 stats: %d \t | \t Mem stats: %d", set.ThreeVolt, set.IMU);
    return ax, ay, az, gx, gy, gz;
}

void PowerMem(char *first_a, char *sec_a){
    if(set.ThreeVolt != 1){
        printf("3V3 stats: %d\r\n(hint: pow 3 1)\r\n", set.ThreeVolt);
    }
    if(first_a == NULL){ 
        printf("No second given <1/0>\r\n");
        printf("hint: To turn temp2 on -> t 2 1\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }	

    // if(sec_a != NULL){
    //     printf("No second argument needed!\r\n");
    //     printf("The second argument will not be applied to the function\r\n");
    //     return;
    // }

    int number = atoi(first_a);

    switch (number)
    {
    case 0: set.IMU = number; break; //printf("IMU sensor: %d\r\n", number);break;
    case 1: set.IMU = number; break; //printf("IMU sensor: %d\r\n", number);break;
    default: printf("Entered incorrect!\r\n(hint: <i><1/0>\r\n"); break;
    }
}

int PowerMem_Test(int number, int stats){
    if(stats < 0 || stats >1) return 0;
    switch (number)
    {
    case 0: return 1; break;
    case 1: return 1; break;
    default: printf("IMU sensor %d does not exist!\r\n", number); return 0; break;
    }
}

/*----------------------------------------------------------------------------------------------------*/
int ReadPressSensors(int number){
    if(set.Press[number] == 1) return set.Press_buf[number] = (rand() % 4096);
    //else printf("5V stats: %d \t | \t Press%d stats: %d", set.FiveVolt, number, set.Press[number]);
    return set.Press_buf[number];
}

void PowerPress(char *first_a, char *sec_a){
    if(set.FiveVolt != 1){
        printf("5V stats: %d\r\n(hint: pow 5 1)\r\n", set.FiveVolt);
    }
    if(first_a == NULL){ 
        printf("No Pressure sensor<1-2> given\r\n");
        printf("hint: To turn presssure sensor 1 on -> p 1 1\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }	

    if(sec_a == NULL){
        printf("No second argument given <1/0>\r\n");
        printf("hint: To turn presssure sensor 1 on -> p 1 1\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }	

    if(strcasecmp(first_a, "a") == 0 || strcasecmp(first_a, "all") == 0){
        set.stats = atoi(sec_a);
        switch (set.stats)
        {
        case 0: for(int i=1; i<(MaxPresSensors); i++) set.Press[i] = set.stats; break; //printf("Pressure sensor %d: %d\r\n", i, (set.Press[i] = set.stats)); break;
        case 1: for(int i=1; i<(MaxPresSensors); i++) set.Press[i] = set.stats; break; //printf("Pressure sensor %d: %d\r\n", i, (set.Press[i] = set.stats)); break;
        default: printf("The second argument can not be less than 0 or greater than 1\r\nTry calling the function again with the correct iput!\r\n"); break;
        }
        return;
    }

    int number = atoi(first_a);
//    set.stats = atoi(sec_a);

    if(set.stats < 0 && set.stats >= 2){
        printf("The second argument is no less than 0 or greater than 1!\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }

    switch (number)
    {
    case 1: set.Press[1] = set.stats; printf("Presssure sensor %d: %d\r\n", number, set.stats);;break;
    case 2: set.Press[2] = set.stats; printf("Presssure sensor %d: %d\r\n", number, set.stats);;break;
    default: printf("The number of the component is no less than 0 or greater than 2!\r\nTry calling the function again with the correct iput!\r\n"); break;
    }
}

int PowerPress_Test(int number, int stats){
    if(set.stats < 0 || set.stats >= 2) return 0;

    switch (number)
    {
    case 1: return 0;break;
    case 2: return 0; break;
    default: printf("Pressure sensor %d does not exist!\r\n", number); return 0; break;
    }
}

/*----------------------------------------------------------------------------------------------------*/
void PowerSensor(char *first_a, char *sec_a){
    if(first_a == NULL){ 
        printf("No power argument number<5/3> given\r\n");
        printf("hint: To turn power 5V off -> pow 5 0\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }

    if(sec_a == NULL){
        printf("No second argument given <1/0>\r\n");
        printf("hint: To turn power 3V3 off -> pow 3 0\r\nTry calling the function again with the correct iput!\r\n");
        return;
    }	

    if(strcasecmp(first_a, "a") == 0 || strcasecmp(first_a, "all") == 0){
        set.stats = atoi(sec_a);
        switch (set.stats)
        {
        case 0: set.ThreeVolt = set.stats; set.FiveVolt = set.stats; break; //printf("3V3: %d\r\n", set.stats); set.FiveVolt = set.stats; printf("5V: %d\r\n", set.stats); break;
        case 1: set.ThreeVolt = set.stats; set.FiveVolt = set.stats; break; //printf("3V3: %d\r\n", set.stats); set.FiveVolt = set.stats; printf("5V: %d\r\n", set.stats);break;
        default: printf("There are only power supply of 5V (5) or 3V3 (3)!\r\nTry calling the function again with the correct iput!\r\n"); break;
        }
        return;
    }

    int number = atoi(first_a);
    set.stats = atoi(sec_a);

    if(set.stats != 1 && set.stats != 0){
        printf("On = 1, Off = 0!\r\n");
        return;
    }

    switch (number)
    {
    case 3: set.ThreeVolt = set.stats; printf("3V3: %d", set.stats);break;
    case 5: set.FiveVolt = set.stats; printf("5V: %d", set.stats);break;
    default: printf("There are only power supply of 5V (5) or 3V3 (3)!\r\nTry calling the function again with the correct iput!\r\n"); break;
    }
}

/*----------------------------------------------------------------------------------------------------*/
void AllSensorsOn(char* first_a, char *sec_a){
    char a[] = "a";
    PowerSensor(a, (char *)"1");
    PowerCalib(a, (char *)"1");
    PowerOdo(a, (char *)"1");
    PowerPress(a, (char *)"1");
    PowerTemp(a, (char *)"1");
    PowerMem((char *)"1", (char *)"1");
}

/*----------------------------------------------------------------------------------------------------*/
void AllSensorsOff(){
    char a[] = "a";
    PowerCalib(a, (char *)"0");
    PowerOdo(a, (char *)"0");
    PowerPress(a, (char *)"0");
    PowerTemp(a, (char *)"0");
    PowerMem((char *)"0", (char *)"0");
    PowerSensor(a, (char *)"0");
}

/*----------------------------------------------------------------------------------------------------*/
int ReadBattery(){
    set.battery_voltage = (rand() % (12-4 + 1)) + 4;
    if(set.battery_voltage < 4.2){
        AllSensorsOff();
    }
    return set.battery_voltage;
}

int ReadBattery_Test(int volt){
    if(volt < 4.2) return 1;
    else return 0;
}


/*----------------------------------------------------------------------------------------------------*/
void ReadData(char *first_a, char *sec_a){
    int val = 0;

    // battery level
    printf("Battery voltage: %d \r\n", ReadBattery());

    // // check if power supply 5V is actief
    // if(set.FiveVolt != 1){
    //     printf("5V stats: %d\r\n", set.FiveVolt);
    //     return;
    // } 

    // caliper
    int result = 0, group = 0;
    for(int i=0; i<=MaxCaliperSensors+1; i++){
        printf("J%d status: %d  value: %d | ", i, set.J[i], ReadCalibSensors(i));
        if((result = i%4) == 3){
            printf("->GROUP %d\r\n", group);
            // for(int j=i-3; j<=i; j++){
            //     val = ReadCalibSensors(j);
            //     printf("J%d: %d |\t", j, val);
            // }
            //printf("\r\n");
            group++;
        }
    }

    // odo
    for(int i=1; i<MaxOdoSensors; i++) printf("Odo %d status: %d\t value: %d | \t", i, set.odo[i], (val = ReadOdoSensors(i)));
    printf("\r\n");

    // press
    for(int i=1; i<MaxPresSensors; i++) printf("Pressure sensor %d status: %d\t value: %d | \t", i, set.Press[i], (val = ReadPressSensors(i)));
    printf("\r\n");

    // // check if power supply 3v3 is actief
    // if(set.ThreeVolt != 1){
    //     printf("3V3 stats: %d\r\n", set.ThreeVolt);
    //     return;
    // }

    // temp
    for(int i=1; i<MaxTempSensors; i++) printf("Temperature sensor %d status: %d\t value: %d| \t", i, set.Temp[i], (val = ReadTempSensors(i)));
    printf("\r\n");

    // IMU
    printf("IMU stats: %d \t Value: %d\r\n", set.IMU, (val = ReadMem()));
    // for(int i=0; i<MaxTempSensors; i++) printf("Temperature sensor %d: %d\t", i, set.Temp[i]);
    //printf("\r\n");

    // POWER 5V AND 3V3
    printf("5V: %d \t | \t 3V3: %d \r\n", set.FiveVolt, set.ThreeVolt);

    // Acti mode
    printf("Activation mode: %d \t | \t Communication mode: %d\r\n", set.activation_mode_flag, set.communication_mode_flag);

    // Commu mode
    printf("Settled RTC-> Time: %d:%d:%d \t Date: %d/%d/%d\r\n", set.hour, set.minutes, set.sec, set.date, set.month, set.year);
}

