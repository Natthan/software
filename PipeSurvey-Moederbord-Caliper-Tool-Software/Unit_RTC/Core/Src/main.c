/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct{
	char *hours, *minutes, *seconds, *day, *month, *year; // pointer to split GetTimeAndDate into hours, minutes and seconds
} Time;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
RTC_TimeTypeDef RtcTime;
RTC_DateTypeDef RtcDate;

uint8_t CompareSeconds;
uint8_t CompareDate;

uint8_t TimeAndDate[64];

int Timeout = 1000; // timeout for UART to wait in ms

uint8_t RX1_Char = 0x00; // variable to store received character

char GetTimeAndDateBuf[20] = {0}; // temporary buffer to store given time and date
int a = 0; // variable to give the value a position in GetTimeAndDate buffer

int uur_int, minuten_int, seconden_int, day_int, month_int, year_int;

bool GotTime = false;
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
Time Set;
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
RTC_HandleTypeDef hrtc;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_RTC_Init(void);
/* USER CODE BEGIN PFP */
void BackupDateToBR(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/*
 * Read data from uart when interrupt occurd
 * return void
 */
void HAL_USART_RxCpltCallback(UART_HandleTypeDef *huart){
    HAL_UART_Receive_IT(&huart2, &RX1_Char, 1); // receive data from uart when interrupt occured
}

/*
 * Check if given parameters are valid
 * return true or false
 */
bool CheckDateAndTime(char CheckTimeandDate[]){
	bool CheckIfValid = true; // check if time and date are valid

	/*-----------------------------------------Check if time is valid--------------------------*/
	Set.hours = strtok(CheckTimeandDate, ":"); // get first argument from buffer CheckTimeandDate and store it as hour, (hh):mm:ss:day:month:year
	uur_int = atoi(Set.hours); // because set.hours is uint8_t type, we convert this in integer to get decimal value
	if(uur_int < 0 || uur_int > 23){ // check if the given hour is between 0 and 23 hour
	  uint8_t HourError[] = "Error: give me hours between 0 and 23\r\n";
	  HAL_UART_Transmit(&huart2, HourError, sizeof(HourError), Timeout);
	  CheckIfValid = false;
	}

	Set.minutes = strtok(NULL, ":"); // get second argument from buffer CheckTimeandDate and store it as minutes, hh:(mm):ss:day:month:year
	minuten_int = atoi(Set.minutes); // converter type uint8_t set.minutes in to int type
	if(minuten_int < 0 || minuten_int > 59){ // check if the given minutes is between 0 and 59 minutes
	  uint8_t MinutesError[] = "Error: give me minutes between 0 and 59 \r\n";
	  HAL_UART_Transmit(&huart2, MinutesError, sizeof(MinutesError), Timeout);
	  CheckIfValid = false;
	}

	Set.seconds = strtok(NULL, ":"); // get third argument from buffer CheckTimeandDate and store it as seconds, hh:mm:(ss):day:month:year
	seconden_int = atoi(Set.seconds); // convert uint8_t into integer
	if(seconden_int < 0 || seconden_int > 59){ // check if the given seconds is between 0 and 59 seconds
	  uint8_t SecondsError[] = "Error: give me seconds between 0 and 59\r\n";
	  HAL_UART_Transmit(&huart2, SecondsError, sizeof(SecondsError), Timeout);
	  CheckIfValid = false;
	}

	/*-----------------------------------------Check if date is valid--------------------------*/
	Set.day = strtok(NULL, ":");  //get forth argument from buffer CheckTimeandDate and store it as day, hh:mm:ss:(day):month:year
	day_int = atoi(Set.day); // convert uint8_t to integer
	if(day_int < 0 || day_int > 31){ // check if the given date is between 0 and 31
	  uint8_t DayError[] = "Error: give me day between 0 and 31\r\n";
	  HAL_UART_Transmit(&huart2, DayError, sizeof(DayError), Timeout);
	  CheckIfValid = false;
	}

	Set.month = strtok(NULL, ":");  //get fifth argument from buffer CheckTimeandDate and store it as month, hh:mm:ss:day:(month):year
	month_int = atoi(Set.month);
	if(month_int < 0 || month_int > 12){
	  uint8_t MonthError[] = "Error: give me month between 0 and 12\r\n";
	  HAL_UART_Transmit(&huart2, MonthError, sizeof(MonthError), Timeout);
	  CheckIfValid = false;
	}

	Set.year = strtok(NULL, ":"); // get sixth argument from buffer CheckTimeandDate and store it as seconds, hh:mm:ss:day:month:(year)
	year_int = atoi(Set.year);
	if(year_int < 21 || year_int > 30){
	  uint8_t YearError[] = "Error: give me year between 21 and 30\r\n";
	  HAL_UART_Transmit(&huart2, YearError, sizeof(YearError), Timeout);
	  CheckIfValid = false;
	}

	day_of_week(day_int, month_int, year_int); // get date with the given parameters

	if(CheckIfValid == false){ // if false the user need to give new date, return false
		uint8_t GivenTime[200] = {0};
		sprintf(GivenTime, "\r\nGiven hour: %d | Given minutes: %d | Given seconds: %d\r\n", uur_int, minuten_int, seconden_int);
		HAL_UART_Transmit(&huart2, GivenTime, sizeof(GivenTime), Timeout);
		return false;
	}
	else{
		uint8_t GivenTime[200] = {0}; // the given data are valid, return true;
		sprintf(GivenTime, "\r\nGiven hour: %d | Given minutes: %d | Given seconds: %d\r\n", uur_int, minuten_int, seconden_int);
		HAL_UART_Transmit(&huart2, GivenTime, sizeof(GivenTime), Timeout);
		return true;
	}
}

/*
 * Calculate the month with the given date
 * return the month
 */
int function_fm(int date, int month, int year) {
   int fmonth, leap;

   //leap function 1 for leap & 0 for non-leap
   if ((year % 100 == 0) && (year % 400 != 0))
      leap = 0;
   else if (year % 4 == 0)
      leap = 1;
   else
      leap = 0;

   fmonth = 3 + (2 - leap) * ((month + 2) / (2 * month))
         + (5 * month + month / 9) / 2;

   //bring it in range of 0 to 6
   fmonth = fmonth % 7;

   return fmonth;
}

/*
 * Calculate the date with the given data
 * Display the date via UART to the user
 * return 0
 */
int day_of_week(int date, int month, int year) {

   int dayOfWeek;
   int YY = year % 100;
   int century = year / 100;

   uint8_t GivenDate[20] = {0};
   sprintf(GivenDate, "Given date: %d/%d/%d \r\n", date, month, year);
   HAL_UART_Transmit(&huart2, GivenDate, sizeof(GivenDate), Timeout);

   dayOfWeek = 1.25 * YY + function_fm(date, month, year) + date - 2 * (century % 4);

   //remainder on division by 7
   dayOfWeek = dayOfWeek % 7;

//   switch (dayOfWeek) {
//      case 0:
//         printf("weekday = Saturday");
//         break;
//      case 1:
//         printf("weekday = Sunday");
//         break;
//      case 2:
//         printf("weekday = Monday");
//         break;
//      case 3:
//         printf("weekday = Tuesday");
//         break;
//      case 4:
//         printf("weekday = Wednesday");
//         break;
//      case 5:
//         printf("weekday = Thursday");
//         break;
//      case 6:
//         printf("weekday = Friday");
//         break;
//      default:
//         printf("Incorrect data");
//   }
   return 0;
}

/*
 * Function to get time
 * return true or false
 */
bool GetTimeFromUart(){
	bool TimeandDate; // check is time and date are valid
	HAL_UART_Receive_IT(&huart2, &RX1_Char, 1); // check constant if interrupt occured
	if(RX1_Char != 0x00){ // if rx1_char buffer is not empty, a character is received from uart
		while(RX1_Char != 0){
			GetTimeAndDateBuf[a++] = RX1_Char; // put this character in buffer
			HAL_UART_Transmit(&huart2, &RX1_Char, sizeof(RX1_Char), Timeout); // echo the given character to the user
			if(RX1_Char == 13){ // if enter is pressed in uart
			  TimeandDate = CheckDateAndTime(GetTimeAndDateBuf); // check the given data
			  uint8_t enter[] = "\r\n"; // put enter in uart
			  HAL_UART_Transmit(&huart2, enter, sizeof(enter), Timeout);
			  a=0; // reset the position in GetTimeAndDateBuf
			  if(TimeandDate == true){ // if the given data is valid
				  uint8_t valid[] = "Much obliged your for your time!\r\n";
				  HAL_UART_Transmit(&huart2, valid, sizeof(valid), Timeout);
				  RX1_Char = 0x00;

				  RtcTime.Hours = uur_int; // set hour
				  RtcTime.Minutes = minuten_int; // set minutes
				  RtcTime.Seconds = seconden_int; // set seconds

				  RtcDate.Date = day_int; // set day
				  RtcDate.Month = ((uint8_t)month_int); // set month
				  RtcDate.Year = year_int; // set year

				  HAL_RTC_SetTime(&hrtc, &RtcTime, RTC_FORMAT_BIN); // set the time with the given hours, minutes and seconds
				  HAL_RTC_SetDate(&hrtc, &RtcDate, RTC_FORMAT_BIN); // set the date with the given day, month and year

				  return true;
			  }
			  else{
				  // TimeandDate == false
				  uint8_t validError[] = "Did not got the time or/and date right!\r\n";
				  HAL_UART_Transmit(&huart2, validError, sizeof(validError), Timeout);
				  RX1_Char = 0x00;
				  return false;
			  }
			}

			RX1_Char = 0x00;
		}
	  RX1_Char = 0x00; // reset the received character
	}
	return false;
}


/*
 * Function to calibrate time and date
 * return none
 */
void CalibrateTimeAndDate(){

	HAL_RTC_GetTime(&hrtc, &RtcTime, RTC_FORMAT_BIN);
	HAL_RTC_GetDate(&hrtc, &RtcDate, RTC_FORMAT_BIN);

	if(RtcTime.Seconds != CompareSeconds)
	{
	  sprintf((char*)TimeAndDate, "Date: %02d.%02d.20%02d Time: %02d:%02d:%02d\n\r", RtcDate.Date, RtcDate.Month, RtcDate.Year, RtcTime.Hours, RtcTime.Minutes, RtcTime.Seconds);
	  HAL_UART_Transmit(&huart2, TimeAndDate, sizeof(TimeAndDate), 100);
	  CompareSeconds = RtcTime.Seconds;
	}
	if(RtcDate.Date != CompareDate)
	{
	  BackupDateToBR();
	  CompareDate = RtcDate.Date;
	}
}

/*
 *
 */
void BackupDateToBR(void)
{
	HAL_RTCEx_BKUPWrite(&hrtc, RTC_BKP_DR2, ((RtcDate.Date << 8) | (RtcDate.Month)));
	HAL_RTCEx_BKUPWrite(&hrtc, RTC_BKP_DR3, ((RtcDate.Year << 8) | (RtcDate.WeekDay)));
}



/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_RTC_Init();
  /* USER CODE BEGIN 2 */
  HAL_UART_Receive_IT(&huart2, &RX1_Char, 1);
  // Greet the user with instructions
  uint8_t greetings[] = "\r\nGive me 24-hour military time and date in hh:mm:ss:day:month:year\r\nfor example: 11:00:00:23:12:21\r\nwhich indicates time = 11:00:00 and the date is 23 december 2021.\r\nCompleet the input with enter\r\n";
  HAL_UART_Transmit(&huart2, greetings, sizeof(greetings), Timeout);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  bool GotTime = false; // check if the input from uart is correct
	  GotTime = GetTimeFromUart(); // return value is between false and true
	  while(GotTime == true){ // if true, stay in the loop while interrupt
		  HAL_UART_Receive_IT(&huart2, &RX1_Char, 1); // check if interrupt occurred
		  if(RX1_Char != 0){ // when interrupt occurred stop the RTC
			  GotTime = false;
		  }
		  CalibrateTimeAndDate(); // function to calibrate time and date
	  }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  //HAL_Delay(100);
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef DateToUpdate = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.AsynchPrediv = RTC_AUTO_1_SECOND;
  hrtc.Init.OutPut = RTC_OUTPUTSOURCE_ALARM;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */

  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date
  */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;

  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  DateToUpdate.WeekDay = RTC_WEEKDAY_MONDAY;
  DateToUpdate.Month = RTC_MONTH_JANUARY;
  DateToUpdate.Date = 0x1;
  DateToUpdate.Year = 0x0;

  if (HAL_RTC_SetDate(&hrtc, &DateToUpdate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */
//  sprintf((char*)Message, "Date: %02d.%02d.20%02d Time: %02d:%02d:%02d\n\r", DateToUpdate.Date, DateToUpdate.Month,
//		  DateToUpdate.Year, sTime.Hours, sTime.Minutes, sTime.Seconds);
//  HAL_UART_Transmit(&huart2, Message, sizeof(Message), Timeout);
  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
